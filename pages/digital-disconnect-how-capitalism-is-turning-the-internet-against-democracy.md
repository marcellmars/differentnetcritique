<!--
.. title: Digital Disconnect: How Capitalism Is Turning the Internet Against Democracy by Robert Waterman McChesney
.. slug: digital-disconnect
.. date: 2017-04-23 16:54:29 UTC+02:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

[DOWNLOAD BOOK](https://library.memoryoftheworld.org/b/ZugmIsGvI_0VE0aDKw94Sf2Uo4hts4-jK0pB3dRRLNaDVQsv)

# The Internet and Capitalism I: Where Dinosaurs Roam?

<span>With the foundation provided in *chapters 2* and *3*, we can turn specifically to an evaluation of the
relationship of the Internet to capitalism. We can also reconsider the
promise of the Internet, as understood by its most fervent advocates in
the late 1980s and early 1990s; these accounts were almost uniformly
optimistic, for legitimate reasons. With all information available to
everyone at the speed of light and impervious to censorship, all
existing institutions were going to be changed for the better. There was
going to be a worldwide two-way flow, or multiflow, a democratization of
communication unthinkable before then. Corporations could no longer
bamboozle consumers and crush upstart competitors; governments and
militaries could no longer operate in secrecy with a kept press spouting
propaganda; students from the poorest and most remote areas would have
access to educational resources once restricted to the elite. In short,
people would have unprecedented tools and power. Not only would there be
information equality and uninhibited instant communication among all
people everywhere, but also there would be access to a treasure trove of
uncensored knowledge that only years earlier would have been unthinkable
even for the world’s most powerful ruler or richest billionaire. Genuine
democracy would become a realistic outcome for the bulk of humanity for
the first time in history. Inequality, exploitation, corruption,
tyranny, and militarism were soon to be dealt their mightiest
blow.</span>

<span>That seems like ten centuries ago. For all of the digital
revolution’s accomplishments, it has failed to deliver on much of the
promise that was once seen as inherent in the technology. The Internet
was expected to provide more competitive markets, accountable
businesses, open government, an [](){#page_97}end to corruption, and
decreasing inequality—or to put it baldly, increased human happiness. It
has been a disappointment. If the Internet actually has improved the
world over the past twenty years as much as its champions once
predicted, I dread to think where the world would be if it had never
existed.</span>

<span>To some extent, the gap between the vision of utopians and reality
can be explained by their failure to appreciate fully that the Internet
would be in direct conflict with hierarchical capitalist powers. James
Curran argues that capitalism shaped the Internet far more than vice
versa, that if the Internet were to remain a public service institution,
it would likely stay on the margins. But leaving the analysis at that
point barely scratches the surface of what has taken place and is taking
place today.</span>

<span>The tremendous promise of the digital revolution has been
compromised by capitalist appropriation and development of the Internet.
In the great conflict between openness and a closed system of corporate
profitability, the forces of capital have triumphed whenever an issue
mattered to them. The Internet has been subjected to the
capital-accumulation process, which has a clear logic of its own,
inimical to much of the democratic potential of digital communication.
What seemed to be an increasingly open public sphere, removed from the
world of commodity exchange, seems to be morphing into a private sphere
of increasingly closed, proprietary, even monopolistic markets. The
extent of this capitalist colonization of the Internet has not been as
obtrusive as it might have been, because the vast reaches of cyberspace
have continued to permit noncommercial utilization, although
increasingly on the margins.</span>

<span>In this *chapter I* assess how capitalism
conquered the Internet—an institution that was singularly noncommercial,
even anticommercial, for its first two decades—in the 1990s and what the
consequences have been subsequently. By capitalism I mean the really
existing capitalism of large corporations, monopolistic markets,
advertising, public relations, and close, collegial, important,
necessary, and often corrupt relationships with the government and the
military. I do not mean the fairy-tale catechism of American politicians
and pundits: heroic upstart little-guy entrepreneurs battling in
competitive free markets while the deadbeat government is on the
sidelines screwing up the job-creating private sector with a lot of
birdbrain liberal regulations. I review how specific real-world powerful
giant corporations in telecommunications and media have responded to the
Internet’s existential challenge to their modus operandi. Could these
dinosaurs survive?</span>

<span>[](){#page_98}Once the Internet was largely turned over to
capitalists to make as much money as possible, the overriding questions
became: What’s the killer app? What firms will emerge that will be the
new Standard Oils and GMs? In *chapter 5* I continue
the analysis, assessing the new digital corporate giants that have come
forth, looking at why and how they tower over not only the Internet but
the whole economy. The key question for most of these new giants has
been: where will the money come from to make this a viable market? The
answer is advertising. I look at how the Internet has been converted
into an advertising-based medium and what that portends for media, not
to mention traditional liberal and democratic values. Throughout
*chapter 5*, the crucial role of government policies
and subsidies in creating and extending the commercial system will be
foregrounded. The chapter concludes with a discussion of how the
military and national security interests in the United States play an
increasingly prominent role in Internet regulation and governance, in a
largely copacetic manner with the corporate giants.</span>

<span>In many respects, *chapters 4* and *5* provide an argument against the capitalist
development of the Internet, though not necessarily capitalism per se. I
draw from a wide body of research on telecommunications, copyright,
monopoly, microeconomics, civil liberties, privacy, and advertising that
has been produced by respected scholars and writers, the vast majority
of whom would be regarded as sympathetic to the market system. Indeed,
some of the critique has been generated by libertarians and
self-described conservatives. Much of the contemporary data I rely upon
comes from the business and trade press and from investment analysts.
That being said, when viewed as a whole, the critique herein does invite
fundamental questions about the overall capitalist system itself, which
are taken up in *chapter 7*, the conclusion.</span>

## So Who Invented the Internet? {#so-who-invented-the-internet .h3}

<span>During the 2000 presidential campaign, Democratic candidate and
vice president Al Gore was repeatedly ridiculed for allegedly claiming
that he “invented” the Internet. The conventional response was along the
lines of “how could any government bureaucrat think that he could have
anything to do with something as entrepreneurial and genius-inspired as
the [](){#page_99}Internet?” Republican candidate George W. Bush
lampooned Gore: “If he was so smart, how come all the Internet addresses
start with W?”^[1](#ch4en1){#rch4en1}^ Of course, the charge against
Gore was false but became an urban legend.^[2](#ch4en2){#rch4en2}^ His
actual claim was merely that as a member of Congress he had played a key
role in channeling funds to support the development of what would become
the Internet.^[3](#ch4en3){#rch4en3}^ The person often regarded as the
father of the Internet defended Gore, with little effect: “VP Gore was
the first or surely among the first of the members of Congress to become
a strong supporter of advanced networking while he served as Senator,”
Vint Cerf stated. “While it is not accurate to say that VP Gore invented
the Internet, he has played a powerful role in policy terms that has
supported its continued growth and application, for which we should be
thankful.”^[4](#ch4en4){#rch4en4}^</span>

<span>This episode demonstrated how quickly the true history of the
Internet had been swallowed up in collective amnesia and replaced by the
mythology of the free market.^[5](#ch4en5){#rch4en5}^ In fact, the
entire realm of digital communication was developed through
government-subsidized-and-directed research during the post–World War II
decades, often by the military and leading research universities. Had
the matter been left to the private sector, the Internet may never have
come into existence.</span>

<span>The story has been told many times, but certain aspects bear
repeating. When computer scientist Paul Baran (not the economist Paul A.
Baran mentioned elsewhere) imagined a decentralized network in the early
1960s, the telephone monopoly AT&T scoffed at his idea, telling him “he
didn’t know how communications worked.”^[6](#ch4en6){#rch4en6}^ The
Internet was designed as an “open and designable technology” through
which scientists could contribute easily in a nonhierarchical
environment. It was unlike the closed systems of corporate
telecommunication, in which private control over the bottleneck, what
Tim Wu has called the master switch, was the basis for
profitability.^[7](#ch4en7){#rch4en7}^ The Internet predecessor,
ARPAnet, under Cerf and Robert Kahn, was designed with no central
control so that the system would be neutral or dumb, leaving the power
to develop specific applications to people on the edges, who could
participate as they wished.^[8](#ch4en8){#rch4en8}^ This “decentralized
control meant all machines on the network were, more or less, peers. No
one computer was in charge.”^[9](#ch4en9){#rch4en9}^ Corporations
accordingly had little interest in the Internet during its formative
decades. IBM declined to even make a bid to provide subnet computers in
1968, saying the venture was not sufficiently
profitable.^[10](#ch4en10){#rch4en10}^ In 1972 the government famously
offered to let AT&T take control of ARPAnet—i.e., [](){#page_100}the
Internet—and the monopolist declined “on the grounds that it would be
unprofitable.”^[11](#ch4en11){#rch4en11}^</span>

<span>The Internet’s origins teach two important lessons. First, basic
research of the kind that generates innovations like the Internet is a
public good, and private sector firms have little incentive to produce
it. Without the pressure to generate returns, as Cerf observed,
government-based research has “the ability to sustain research for long
periods of time.”^[12](#ch4en12){#rch4en12}^ Moreover, corporate
research labs “rarely if ever invest in fundamental technology that will
likely undermine the economic dominance they currently
enjoy.”^[13](#ch4en13){#rch4en13}^ In corporate-think, the proper role
of the government goes like this: make the massive initial investments
and take all the risk. Then, if and when profitable applications become
apparent, let commercial interests move in and rake in the chips, soon
followed by shamelessly denouncing government taxation and regulation as
interference with the productive work of the private sector. Another
approach would accept that government investment in research is
desirable and necessary, but argue that the public, through the
government, deserves to get the same sort of deal for *its* investment
from commercial interests that private investors would expect if they
had bankrolled all the initial research and development and assumed all
the risk when the risk was greatest.^[14](#ch4en14){#rch4en14}^ This
return for the public is what was effectively being negotiated (and
given away) in the 1990s when the Internet was turned over to the
private sector.</span>

<span>Second, the Internet experience highlights the utterly central
role that military spending has played in bankrolling technology (and
economic development) in the United States since the 1940s. By one
study, since 1945 fully one third of U.S. research professors have been
supported by national security agencies.^[15](#ch4en15){#rch4en15}^
“Across the spectrum of high-technology industries,” Nathan Newman
writes, “the single overwhelming factor correlating with the rise of
technology firms in any region is the level of defense
spending.”^[16](#ch4en16){#rch4en16}^ This is particularly true for
communication. The U.S. Air Force, for example, did the research in the
early 1960s that provided the basis for the personal computer and the
mouse.^[17](#ch4en17){#rch4en17}^ Likewise, the basic architecture of
computer design, advances in time-sharing minicomputers, and most of
networking technology were the result of military spending and “massive
government support.”^[18](#ch4en18){#rch4en18}^ As John Hanke, an
Internet CEO and one of the creators of Google Earth, put it, “The whole
history of Silicon Valley is tied up pretty closely with the military.”
Google Earth specifically would not exist [](){#page_101}unless the
military had been “willing to pay millions of dollars per user to make
it possible.” “We’ve come to the point,” Peter Nowak writes, “where it’s
almost impossible to separate any American-made technology from the
American military.”^[19](#ch4en19){#rch4en19}^ Nor is this ancient
history. In 2012 *Wired* magazine’s Chris Anderson provided a cover
story describing the extraordinary potential benefits of drone warfare
for communication and society: “This new generation of cheap, small
drones is essentially a fleet of flying smartphones,” he
enthused.^[20](#ch4en20){#rch4en20}^ Military spending on research and
development is such a central part of American capitalism that it is
almost impossible to imagine the system existing without it.</span>

<span>The total amount of the federal subsidy of the Internet is
impossible to determine with precision. As Sascha Meinrath, a leading
policy expert at the New America Foundation, puts it, calculating the
amount of the historical federal subsidy of the Internet “depends on how
one parses government spending—it’s fairly modest in terms of direct
cash outlays. But once one takes into account rights of way access that
were donated and the whole research agenda (through the Defense Advanced
Research Projects Agency, the National Science Foundation, etc.), it’s
pretty substantial. And if you include the costs of the wireless
subsidies, tax breaks (e.g., no sales taxes on online purchases), etc.,
it’s well into the hundreds of billions
range.”^[21](#ch4en21){#rch4en21}^ Meinrath does not even include the
immense amount of volunteer labor that provided a “continuous stream of
free software to improve its functionality.”^[22](#ch4en22){#rch4en22}^
For context, even a conservative take on Meinrath’s estimate puts the
federal investment in the Internet at least ten times greater than the
cost of the Manhattan Project, allowing for
inflation.^[23](#ch4en23){#rch4en23}^</span>

<span>The issue is not only public subsidies with no return on
investments. It’s also about a public ethos. The early Internet was not
only noncommercial, it was anticommercial. Computers were regarded by
many of the 1960s and ’70s generation as harbingers of egalitarianism
and cooperation, not competition and profits. Apple’s Steve Wozniak
recalls that everyone at his 1970s computer club “envisioned computers
as a benefit to humanity—a tool that would lead to social
justice.”^[24](#ch4en24){#rch4en24}^ Salvador Allende’s democratic
socialist government in Chile in the early 1970s devoted considerable
resources to computing, in the belief that it could provide efficient
economics without the injustice and irrationality of
capitalism.^[25](#ch4en25){#rch4en25}^ By the 1970s and 1980s, the
computer professionals and students who comprised the Internet community
“deliberately cultivated an open, non-hierarchical culture that imposed
[](){#page_102}few restrictions on how the network could be
used.”^[26](#ch4en26){#rch4en26}^ Rebecca MacKinnon calls this the
digital commons, which would provide the foundation for all subsequent
commercial applications.^[27](#ch4en27){#rch4en27}^ The hacker culture
that emerged in that period was typified by its commitment to
information being free and available, hostility to centralized authority
and secrecy, and the joy of learning and
knowledge.^[28](#ch4en28){#rch4en28}^</span>

<span>Nothing enraged the Internet community more than advertising and
commercialism. Prior to the early 1990s, the National Science Foundation
Network (NSFNet), the immediate forerunner of the Internet, explicitly
limited the network to noncommercial uses. The first commercial e-mail
message, which gained considerable attention, was sent in April 1994 to
every board in the massive Usenet system, which held the noncommercial
Internet culture near and dear to its heart.^[29](#ch4en29){#rch4en29}^
The sender of that e-mail was flamed by countless Usenet users, meaning
that they clogged the advertiser’s inbox with contemptuous messages
demanding that the sales pitch be removed and such conduct never be
repeated. This internal policing by Internet users was based on the
assumption that commercialism and an honest, democratic public sphere do
not mix. Advertising already saturated the balance of the mass media; it
wasn’t as if people couldn’t find enough commercials. The Internet was
to be the one place where citizens could seek refuge and escape the
incessant sales pitch.</span>

<span>This contempt for digital commercialism was well understood in the
business community. In 1993 the trade publication *Advertising Age*
lamented how the Internet is encased in a culture “which is loathe
\[sic\] to advertising.”^[30](#ch4en30){#rch4en30}^ Marketers feared
that their efforts to use the Web would be greeted by a tidal wave of
flaming from “a cyberspace community peopled by academics and
intellectuals” who regarded a commercialized Internet as “advertising
hell.”^[31](#ch4en31){#rch4en31}^ As late as 1998, Google founders Larry
Page and Sergey Brin rejected the idea that their search engine should
be supported by advertising. “We expect that advertising funded search
engines will be inherently biased towards the advertisers and away from
the needs of consumers,” they wrote. “The better the search engine is,
the fewer advertisements will be needed by the consumer to find what
they want.”^[32](#ch4en32){#rch4en32}^ The Internet was expected, as
Madison Avenue feared deeply, to make advertising irrelevant and
obsolete.</span>

<span>As the Internet grew, it gradually attracted the interests of
commercial concerns keen to see how they might capitalize upon it.
Perhaps the first skirmish came with e-mail, which was created in 1972
by a hacker to piggyback [](){#page_103}on the file transfer protocol.
With the support of ARPAnet, e-mail soon surpassed all other forms of
computer resource sharing.^[33](#ch4en33){#rch4en33}^ It was the first
“killer app.” By the end of the decade, the U.S. Postal Service proposed
the creation of an electronic mail service that it would administer, at
first for business customers who expressed an interest, and then to
others as the system developed. Even then, postal analysts argued that
“the Postal Service must enter and participate vigorously in an
electronic mail system if it is to survive.” Perhaps a decade earlier,
this might have flown, but during the Reagan administration, business
opposition from firms like AT&T was sufficient to quash the
proposal.^[34](#ch4en34){#rch4en34}^ Had the Postal Service been
successful, it might have put the Internet on a rather different
trajectory as a decidedly nonprofit public service
medium.^[35](#ch4en35){#rch4en35}^ By 1982 Cerf left the government and
was working for the telecommunication company MCI, where he put together
the first commercial e-mail system. When the MCI e-mail system was
formally attached to the Internet in 1989, the commercial Internet was
born.^[36](#ch4en36){#rch4en36}^</span>

<span>Another commercial application of the Internet was the wave of
online computer services—proprietary networks like America Online,
Compu-Serve, and Prodigy—that provided “walled gardens” where the
services controlled the content. These services enjoyed brief success,
but they all failed after the emergence of the World Wide Web in the
1990s provided for free an infinitely greater amount of material than
these services provided for a fee.^[37](#ch4en37){#rch4en37}^ AOL
managed to survive and prosper by providing dial-up Internet access,
rather than a walled-off system, in the prebroadband era of the late
1990s.</span>

<span>The single greatest concern in the Internet community during this
period was the growth of patents and efforts by commercial interests to
make proprietary what had once been open and free. As commercial
interests were seen taking an increasing interest in the Internet, there
was, as James Curran has documented, a “revolt of the nerds”—led by
people like Richard Stallman and Linus Torvalds—which launched the
open-software movement in the 1980s.^[38](#ch4en38){#rch4en38}^ Much of
the noncommercial institutional presence on the Internet today can be
attributed to this movement and its progeny. When Tim Berners-Lee
created the World Wide Web in 1990, he said it would have been
“unthinkable” to patent it or ask for fees. The point of the Internet
was “sharing for the common good.” That was about to change. As the
market exploded in the 1990s, patents became the rage. The use of
patents to create unnecessary and dangerous monopolies rather than as
incentives for [](){#page_104}research, as Berners-Lee put it, became a
“very serious problem.” By 1999 he wondered openly if the Internet was
becoming a “technical dream or a legal
nightmare.”^[39](#ch4en39){#rch4en39}^</span>

<span>During the 1990s, the Internet was transformed from a public
service to a distinct, even preeminent, capitalist sector. The Internet
was formally privatized in 1994–95 when the NSFNet turned the backbone
of the Internet over to the private sector. Thereafter market forces
were to determine its course. The transition culminated a good six years
of mostly secret high-level deliberations involving government and the
private sector. Compared to the political debate that surrounded the
emergence of radio broadcasting in the 1930s or the uprising against
Western Union’s telegraph monopoly in the late nineteenth century, there
was nary a trace of popular discussion about whether this privatization
and commercialization was appropriate and what its implications might
be. Press coverage was nonexistent, so the general public did not have a
clue; the media watch group Project Censored ranked the privatization of
the Internet as the fourth most censored story of 1995. The number-one
most censored story was that of the deliberations leading up to what
would become the Telecommunications Act of
1996.^[40](#ch4en40){#rch4en40}^</span>

<span>Why was there no organized or coherent opposition? In view of the
dominant noncommercial ethos that had driven the Internet and had been
one of its most attractive features before 1995, the lack of opposition
is striking. In my view, there were four crucial factors that account
for the uncontested triumph of a privatized Internet.</span>

<span>First, the point emphasized in [chapter 3](Chapter003.html) came
into play: the policy-making process throughout the 1990s (and beyond)
was dominated by large corporations and their trade associations. The
traditional pattern was for the government to develop new communication
technologies and then turn them over to capitalists once they could make
profits. What little debate there was concerned what (generally minor)
public interest obligations would be attached to the gift. Press
coverage was restricted to the business press, so the general public had
virtually no idea what was taking place. Politicians in both parties
benefited by relations with the massive incumbents, who basically owned
the board in Washington, D.C., and the state capitals. Due to the
imbalance of power in these negotiations, the benefits invariably
redounded to private interests. The Internet posed a distinct
existential challenge to numerous superpowerful corporations and
industries, as well as almost unimaginable promise to them and other
businesses. They were not about to disappear [](){#page_105}quietly for
the good of humanity and allow a fair, widespread public discussion on
how best to deploy digital technologies to enhance democracy, the
economy, and quality of life.</span>

<span>Second, there was no single policy or coherent set of policies
that determined the nature of the Internet. The implications of the
1994–95 privatization were not at all clear. Numerous crucial policy
changes and technological advances would be required to get the Internet
to where it is in 2013, none of which could have been anticipated in the
1990s. There were numerous institutions, industries, and government
agencies that had a role in how the Internet would develop, and none was
all-powerful. It was very difficult to get a handle on it. The threat
that the Internet would be taken over by Big Brother or Rupert Murdoch
seemed remote. For activists concerned about what to do to protect the
public interest and prevent corporate domination online, it was
difficult to get a sense of what policies were going to be effective. If
this was a critical juncture—and many in the 1990s understood it as
exactly that—it was not at all clear what the exact issues and
alternatives were. Plenty of room existed in the infinite digital realm
for everyone to “do their thing,” so it seemed that commercial and
noncommercial users could easily co-exist. The only clear policy concern
was to prevent explicit government censorship of Internet speech, as
exemplified by the Communications Decency Act of 1996, which was almost
immediately ruled unconstitutional.</span>

<span>There was also an element of arrogance among hackers, who tended
to believe that no matter what the corporate guys cooked up, they would
be able to circumvent it. The revolutionary nature of the technology
could trump the monopolizing force of the market. This might help
explain the 1990s alliance of sorts forged between some prominent
counterculture types who embraced the Internet, like Stewart Brand, John
Perry Barlow, and Esther Dyson, with free-market ideologues and
techno-enthusiasts, like George Gilder and Newt
Gingrich.^[41](#ch4en41){#rch4en41}^</span>

<span>Third, even in the context of a corporation-dominated polity, the
political culture of the 1990s was close to an all-time high for
procapitalist sentiment and close to an all-time low for notions of
public service or regulation. The notions of public goods and regulation
in the public interest were suspect, if not ridiculed. The digital
revolution exploded at precisely the moment that what is commonly known
as neoliberalism was in ascendance, its flowery rhetoric concerning
“free markets” most redolent. The dynamism of technological revolution
imbued the power-grab of corporations with the patina of
[](){#page_106}moral rectitude, virtue, and public service. The core
opinion was that businesses should *always* be permitted to develop any
area where profits could be found and that this was the most efficient
use of resources for an economy.^[42](#ch4en42){#rch4en42}^ Digital
technology supercharged the free-market mantra as it undermined the case
for regulation of oligopolistic and monopolistic industries. New
technologies created new competition, the argument went, and let the
market work its magic.</span>

<span>Conversely, anything smacking of the left was suspect. Democrats,
with a renewed commitment to free enterprise, were running full-speed
away from the term *liberal.* They could do so with impunity because
their base would be unlikely to vote Republican. President Clinton
proclaimed, “The era of big government is over.” Anything interfering
with capitalist expansion was seen as bad economics and ideologically
loaded, advanced by a deadbeat “special interest” group that could not
cut the mustard in the world of free-market competition and so sought
protection from the corrupt netherworld of government regulation and
bureaucracy.^[43](#ch4en43){#rch4en43}^ This credo led to the drive for
deregulation throughout the economy and for the privatization of many
once public-sector activities. The Clinton administration and
Republicans were in sync when it came to the Internet: As Clinton and
Gore put it in their 1997 *Framework for Global Electronic Commerce*,
the first principle was that “the private sector should lead.”
Electronic commerce “over the Internet should be facilitated on a global
basis.” Matthew Crain’s trailblazing dissertation research on this
period demonstrates that the Clinton administration worked extensively,
quietly, and harmoniously with private industry on all policy matters
related to the Internet, while incipient public interest concerns about
privacy and advertising were marginalized. When controversial issues
emerged, the preferred solution was industry
self-regulation.^[44](#ch4en44){#rch4en44}^</span>

<span>The symbolic coup de grâce for the public interest came in the
bipartisan 1996 Telecommunications Act. This bill dealt with the
Internet only indirectly, and was mostly the result of a turf war
between the regional Bell monopolies (the “Baby Bells”) and the
long-distance carriers. Derek Turner of Free Press argues, in fact, that
a careful reading of the act reveals that it included several measures
that could have spawned more competition and advanced the public
interest in the digital realm. The real tragedy was that Congress washed
its hands of fundamental policy making at this point, ending the
possibility of meaningful public involvement. Matters were turned over
to endless court challenges of public interest provisions in the law
[](){#page_107}brought by the corporate players, and to the Federal
Communications Commission (FCC), which in the dark of night had little
compunction about serving the needs of large corporate interests as they
looked to exploit the Internet. “Before the ink was even dry on the 1996
Act,” Turner writes, “the powerful media and telecommunications giants
and their army of overpaid lobbyists went straight to work obstructing
and undermining the competition the new law was intended to create. By
the dawn of the twenty-first century, what they could not get overturned
in the courts was gladly undone by a new FCC staffed and led by the same
lobbyists.”^[45](#ch4en45){#rch4en45}^</span>

<span>In public pronouncements that accompanied the 1996
Telecommunications Act, this addition to communication law was premised
on the notion that traditional concerns with “natural monopoly” in
telecommunications and concentrated markets in media were rendered moot
by the Internet, which would unleash so much new competition that there
was no longer any justification for regulation. The propaganda was so
thick, no one stopped to ask why huge monopolistic firms would be
lobbying for deregulation if it would leave them facing increased
competition and therefore reduced profits.</span>

<span>The biggest lie of deregulation was that these were markets that
the government could exit, allowing market competition to work its
magic. On the contrary, all the communication markets—including
telephone, cable and satellite TV, broadcasting, motion pictures, and
recorded music—were created or decisively shaped by the government and
based on government monopoly licenses or privileges. Deregulation did
not remove the government or the importance of policy making by one
iota. In every area of importance, the government still played a central
role. What deregulation did was remove or severely lessen the idea of
government action in the public interest. The point of government
regulation, pure and simple, became to help firms maximize their
profits, and that was the new public interest. Deregulation in
communication meant, in effect, “re-regulation strictly to serve the
largest corporate interests.” “If the present trend is not
comprehensively interrupted,” Dan Schiller perceptively wrote in 1999,
“the extent to which cyberspace becomes a commercial consumer medium
will be very largely determined by profit-seeking companies
themselves.”^[46](#ch4en46){#rch4en46}^ This reregulation let companies
locate the most profitable uses and then build policies to support those
activities.</span>

<span>There was a fourth factor that undermined opposition or even
debate [](){#page_108}despite this blatant pandering to a handful of
corporations. The Internet bubble of the late 1990s made policies
promoting the commercial development of cyberspace seem not only
appropriate, but brilliant. After a difficult recession in the early
1990s following a scary crash in 1987, the Internet-inspired New Economy
seemed to be the solution to the growth problems of capitalism. The late
1990s were a giddy moment, and the U.S. news media could barely contain
themselves with their enthusiasm for the happy couple. Capitalism and
the Internet seemed a marriage made in
heaven.^[47](#ch4en47){#rch4en47}^ The emerging CEOs were the conquering
heroes of the time, visionary seers, world-historical geniuses, and men
of action, fully deserving of their rewards. “I think Bill Gates has the
right to make \$50 billion,” Harvard’s Henry Louis Gates Jr. stated in
1998, “if he’s smart enough to figure all that stuff
out.”^[48](#ch4en48){#rch4en48}^ Where the hell did Al Gore get off
thinking he deserved credit for the work of these titans?</span>

<span>Although the policy battles were won decisively by capital, it is
important to acknowledge that there remained plenty of space for people
to use the Internet as they wished, so this was not a case like radio
broadcasting, wherein the system was turned over to a small number of
commercial interests as a monopoly. There has been a tremendous burst of
nonprofit and noncommercial Internet sites and free or open software and
applications—Yochai Benkler puts the number in the thousands—that have
become a central part of the digital realm as experienced by many
online.^[49](#ch4en49){#rch4en49}^ *Wikipedia* is the most striking
example. As John Naughton puts it, amateurs “have created what is
effectively the greatest reference work the world has yet
produced.”^[50](#ch4en50){#rch4en50}^ *Wikipedia* founder Jimmy Wales
understood from the outset that it could not be credible and successful
if it was commercial, and *Wikipedia* still has a stance toward
advertising that conjures up the Net’s salad
days.^[51](#ch4en51){#rch4en51}^</span>

<span>At their best, these noncommercial cooperative ventures hark back
to what Internet celebrants have most extolled about the technology’s
virtues and potential.^[52](#ch4en52){#rch4en52}^ The most prominent of
these developments have found a niche that sits comfortably with the
dominant commercial players. As Rebecca MacKinnon puts it, “Open-source
software is not inherently anti-business,” and many of the giants like
Google use it where it helps them.^[53](#ch4en53){#rch4en53}^ “Many
businesses built on top of open source,” notes James Losey of the New
America Foundation, “such as Apple building its OSX on top of
Unix.”^[54](#ch4en54){#rch4en54}^ Google and *Wikipedia*, as Siva
Vaidhyanathan writes, have such a strong synergy—*Wikipedia* ranks near
the top of most Google searches—that “it’s unlikely [](){#page_109}any
reference source would unseat Wikipedia.”^[55](#ch4en55){#rch4en55}^
This cooperative sector is important for the corporate players too,
because it brings legitimacy to the commercial Internet as something
more than a digital ATM for billionaires. When Mark Zuckerberg prepared
the initial public offering for Facebook in 2012, he wrote to potential
investors that Facebook “was not originally created to be a company.”
Instead, “it was built to accomplish a social mission—to make the world
more open and connected.”^[56](#ch4en56){#rch4en56}^</span>

## Internet Service Providers: From Monopoly to Cartel? {#internet-service-providers-from-monopoly-to-cartel .h3}

<span>Two of the industries most immediately threatened by the Internet
were the telephone and the cable television industries. For many
generations the giant telephone and, to a lesser extent, cable TV firms
had been the recipients of enormous indirect government subsidies
through their government monopoly franchises. Almost all of them
operated with local monopolies. Although often unpopular with consumers,
they were arguably the most extraordinary lobbying force in the nation,
as their survival depended on government authorization and
support.</span>

<span>The great challenge for these industries was to survive the
digital revolution. It seemed to be only a matter of time until the
Internet provided all sorts of voice communication and access to all
sorts of audiovisual entertainment at virtually no cost, making both of
these industries superfluous or at least far smaller and less
profitable. They were able to meet the challenge through their unrivaled
political muscle not only in Washington, but in state and municipal
governments as well. Their great leverage was due to the fact that,
thanks to government-created monopolies, they controlled the wires
necessary for Internet access, at least until a more sophisticated,
wireless system could be constructed. The telephone companies had lent
their wires to Internet transmission, and in the 1990s, they—soon
followed by the cable companies—realized that the wires were their
future, and a lucrative one at that. But there were crucial political
victories that needed to be won first, and it was not at all clear that
they would win them.</span>

<span>The first threat to these firms was the new competition that was
going to arrive with the ownership deregulation inscribed in the
Telecommunications Act of 1996. There were roughly a dozen major
telephone companies in the mid-1990s, some long-distance firms, and
seven regional phone [](){#page_110}monopolies resulting from AT&T
having been split up in 1984. There were another eight or so major cable
TV and satellite TV companies, each of the cable TV providers having a
monopoly license where it operated, and each satellite TV firm had
monopoly rights to part of the electromagnetic spectrum. The theory was
that with digital communication, all these firms would leave their
monopoly boundaries and begin to compete with each other and that phone
and cable and satellite TV companies would go after one another’s
business. In addition, all sorts of new players were certain to enter
the field now that the official monopoly licenses were ending and the
digital gold mine was in sight. Images of the Wild West Internet were
invoked to suggest an onslaught of new competitors in telecommunication
and cable/satellite television. The principle was “competition
everywhere,” creating what Tim Wu called a “Hobbesian struggle of all
against all.”^[57](#ch4en57){#rch4en57}^</span>

<span>These telephone and cable giants came to tolerate and eventually
support the long process of “deregulation” of their industries that came
to a head in the 1990s, not because they eagerly anticipated ferocious
new competition, but because they suspected that the new regime would
allow them to grow ever larger and have more monopolistic
power.^[58](#ch4en58){#rch4en58}^ It was a cynical moment. The stated
justification for deregulation was that these traditional phone and
cable monopolies would be permitted to use their wires to compete with
each other in local markets, creating bona fide competition. In
exchange, restrictions on mergers would be relaxed, so the helpless
giants could gird themselves for the coming competitive
Armageddon.</span>

<span>It was all nonsense. The powerful incumbent players had sufficient
monopoly power, commercial and political, to ensure that no new serious
competitors emerged. In Texas, for example, SBC (the Baby Bell that
later reconstructed AT&T) had nearly a hundred registered lobbyists
working on a legislature of 181 members. Not surprisingly Texas passed
laws making it very difficult for any newcomer to challenge SBC’s
telephone monopoly.^[59](#ch4en59){#rch4en59}^ In most cases, the
dominant players knew it was in their interest not to mess with another
incumbent, and outsiders realized it was tantamount to torching their
capital to try to break into these industries. The upshot has been a
wave of massive mergers shrinking the number of telephone and cable
powerhouses down to between six and ten, depending on one’s
criteria—less than half the total from the mid-1990s—with AT&T, Verizon,
and Comcast emerging as dominant and much more profitable
entities.</span>

<span>Deregulation has led to the worst of both worlds: fewer enormous
firms [](){#page_111}with far less regulation. To top it off, the
political power of these firms in Washington and state capitals has
reached Olympian heights.^[60](#ch4en60){#rch4en60}^ Accordingly,
politicians pretty much ignored their platitudes about increasing
competition. The George W. Bush administration, as Tim Wu puts it,
“tended to agree that competition didn’t necessarily require that there
be any extant competitors.”^[61](#ch4en61){#rch4en61}^ These monopolists
are the poster children for crony capitalism, which in theory promarket
types despise but in practice invariably champion, at least when they’re
anywhere near political power.</span>

<span>Increasing monopoly power and crushing the threat of competition
was all well and good, but it did not solve the problem posed by the
Internet. The telephone companies provided the main wires for Internet
access in the late 1990s, but the FCC required that they fulfill the
“common carriage” statutes, which meant that the shrinking number of
Baby Bells had to allow open access at nondiscriminatory prices for
other firms to use their lines as Internet service providers (ISPs).
This led to an explosion in the number of ISPs—it was a highly
competitive market—in which AOL rose to the top by the end of the
decade. The telephone companies despised this regulation, and through
the courts and regulatory system they pushed to have it ended, so they
could have exclusive rights to use their own networks for ISP
purposes.^[62](#ch4en62){#rch4en62}^ Otherwise, their future was pretty
grim, renting out their dumb pipes for other people to use to get rich,
especially as telephony would switch over to Internet protocols.</span>

<span>By the new decade, the fat cable pipes were brought online to
provide broadband Internet access. The cable companies, too, initially
had to follow the common-carrier provisions that telephone companies
faced. Then, in 2002—very quietly, and with no debate or public hearing
and scarcely a scintilla of news media coverage—the Bush
administration’s FCC reclassified cable modems as an information
service, rather than a telecommunication service. It was a party-line
decision, with Democrat Michael Copps providing the lone dissenting
vote.^[63](#ch4en63){#rch4en63}^ This change allowed cable to escape the
common-carrier provisions. A cable company could be the *only* ISP to
use its wires. The U.S. Supreme Court upheld the FCC’s constitutional
right to make this reclassification—though not necessarily agreeing with
what the FCC did—in the 2005 *NCTA v. Brand X* case. Shortly thereafter,
the FCC reclassified the phone companies’ Internet access services as
information services, so they, too, could avoid the open-access
requirement. By this time, nearly 50 percent of independent ISPs had
already gone out of business [](){#page_112}since 2000; and soon nearly
all of the rest would follow. As a leading study put it, “Broadband
competition in the United States has
collapsed.”^[64](#ch4en64){#rch4en64}^ A crucial policy fight going
forward is getting the FCC to reverse its 2002 decision and return both
cable and telephone-based broadband to the telecommunication service
classification.</span>

<span>This reclassification has had magnificent consequences for the
bottom lines of a handful of telecommunication giants and disastrous
implications for broadband development in the United States. Nearly 20
percent of U.S. households have access to no more than a single
broadband provider—a monopoly. Using FCC data (that the commission
acknowledges probably overstates the degree of actual competition), all
but 4 percent of remaining households has, at most, two choices for
wired broadband access, a duopoly comprised of the local monopoly
telephone provider—which may or may not be aggressively pushing wireline
broadband—and a cable company.^[65](#ch4en65){#rch4en65}^ There is no
incentive for these duopolists to expand the market if it means they
must lower their monopoly prices and profits; hence the persistence of
the “digital divide,” which I turn to below. The Obama administration
set aside \$7.2 billion in stimulus money to bring high-speed Internet
to underserved areas, and while it helped in some rural areas, “it had
no impact on the broader competitive situation in the market for most
American consumers.”^[66](#ch4en66){#rch4en66}^ Moreover, it gave the
big players an indirect subsidy “because a lot of the projects that were
supported then need to buy connectivity from the major
telcos.”^[67](#ch4en67){#rch4en67}^</span>

<span>The other great development is the rise of cell phones,
smartphones, and wireless Internet access. With extraordinary corporate
jujitsu and no public study or debate, the handful of old telephone
companies have gobbled up spectrum and transitioned to becoming the
dominant cell phone and wireless ISP providers. Four companies control
around 90 percent of the U.S. wireless market, and two of them—AT&T and
Verizon—control 60 percent of the market and have more than 90 percent
of free cash flow.^[68](#ch4en68){#rch4en68}^ It has become a classic
duopoly, in which the smart play is to imitate the other firm. “AT&T and
Verizon don’t really compete with one another,” a Consumers Union
attorney commented. “They copy one another.”^[69](#ch4en69){#rch4en69}^
So it was that when AT&T put limits on the data customers were allowed
to download in 2012, Verizon quickly followed
suit.^[70](#ch4en70){#rch4en70}^ A few months later, Verizon announced a
new scheme to allow its customers to purchase a certain amount of
wireless data capacity that could be spread across a family’s digital
machines. [](){#page_113}The plan was devised to maximize Verizon’s
revenues. “Verizon is finally delivering something everybody wants,” a
telecommunication analyst at the research firm Ovum said, “in a way
nobody wants.” AT&T was expected to offer an almost identical plan in
short order.^[71](#ch4en71){#rch4en71}^</span>

<span>What is the price of duopoly? *Americans pay an average of \$635
per year for cell phone service; people in Sweden, the Netherlands, and
Finland pay less than \$130 for superior service.* Moreover, in 1997
cell phone firms invested a whopping 50 percent of every dollar of
revenue in the cell phone network; today, with little competitive
pressure, capital expenditures have fallen to 12.5 percent of
revenues.^[72](#ch4en72){#rch4en72}^ “AT&T doesn’t want to invest more
in this network than it absolutely has to,” telecommunication expert
Susan Crawford explains. “Building more towers and connecting all of
them to fiber would bring down the value of its
shares.”^[73](#ch4en73){#rch4en73}^ It’s a textbook case of monopoly
power, and it pays off. In 2011, AT&T and Verizon were the twelfth and
sixteenth largest firms in the Fortune 500, with combined revenues of
\$230 billion and combined profits of \$20
billion.^[74](#ch4en74){#rch4en74}^</span>

<span>The concern is not simply that each of the wired broadband and
wireless realms are monopolistic; it is also that they are becoming what
Harold Feld of Public Knowledge terms a
cartel.^[75](#ch4en75){#rch4en75}^ In 2011 and 2012, exclusive deals
were engineered between the dominant cable companies and the
telecommunication companies so they would work closely together on
standards and integrate their services.^[76](#ch4en76){#rch4en76}^ To
some extent, they were made in recognition of the fact that cable had
won the battle for wired broadband. By 2011, 75 percent of people adding
broadband were choosing cable.^[77](#ch4en77){#rch4en77}^ Cable firms
agreed to give up their spectrum so the cell phone companies could have
more of it, while the cell phone companies effectively withdrew from
serious competition for wired broadband customers. The major development
was when cable powerhouse Comcast and wireless giant Verizon reached a
deal to market each other’s services in December 2011, in the midst of a
number of similar deals among the big
players.^[78](#ch4en78){#rch4en78}^ As Feld puts it, “these side
agreements amount to a tacit agreement to divide up markets between them
and avoid competition.”^[79](#ch4en79){#rch4en79}^</span>

<span>In August 2012, the cartelization of the entire ISP market—wired
and wireless—was effectively sanctioned when both the Justice Department
and the FCC approved Verizon’s deal to swap spectrum, divvy up the
market, and collaborate with Comcast and the other major cable
companies.^[80](#ch4en80){#rch4en80}^ “These companies aren’t competing
anymore. Now they’re partners,” [](){#page_114}telecom industry analyst
Jeff Kagan said. “All I see is bad.”^[81](#ch4en81){#rch4en81}^ “Instead
of an arms race between telephone and cable incumbents,” tech policy
journalist Timothy Lee wrote, “we seem to be getting a
truce.”^[82](#ch4en82){#rch4en82}^ Providers, Feld notes, can
aggressively pursue “anticonsumer incentives with no
consequence.”^[83](#ch4en83){#rch4en83}^ Considering the size and
importance of the telecommunication and ISP markets to the economy, that
is an extraordinary state of affairs.</span>

<span>The consequences of the monopoly system are evident. In 2000 the
United States was a world leader in terms of broadband penetration and
access, “12–24 months ahead of any European country,” according to the
Danish National IT and Telecom Agency.^[84](#ch4en84){#rch4en84}^ Today
the United States ranks between fifteenth and thirtieth in most global
measures of broadband access, quality of service, and cost per
megabit.^[85](#ch4en85){#rch4en85}^ In a September 2011 global report
from Pando Networks, the United States ranked twenty-sixth in the world
in average consumer download speed.^[86](#ch4en86){#rch4en86}^ A 2012
New America Foundation examination of twenty-two cities worldwide
concluded that “U.S. consumers in major cities tend to pay higher prices
for slower speeds compared to consumers
abroad.”^[87](#ch4en87){#rch4en87}^ “Here’s a big fact,” the author of
the FCC’s National Broadband Plan, Blair Levin, stated in 2012: “For the
first time since the beginning of the commercial Internet, the United
States does not have a commercial wireline provider with plans to build
a better network than the currently best available
network.”^[88](#ch4en88){#rch4en88}^ Crawford notes that this means most
Americans will never get access to “the speeds the rest of the world is
used to.”^[89](#ch4en89){#rch4en89}^ The New America Foundation places
the direct cost of monopoly in wireline to American consumers over the
next decade at \$250 billion.^[90](#ch4en90){#rch4en90}^</span>

<span>There is more than a little tragedy surrounding the emergence of
the cartel. There exists a great deal of existing and potential unused
spectrum that could be used to establish a superb wireless network
alternative to the existing cartel and drive down
prices.^[91](#ch4en91){#rch4en91}^ As *The Economist* notes, the unused
spectrum may offer a “third pipe” that can “rival cable and telephone
broadband for access to the Internet.”^[92](#ch4en92){#rch4en92}^ What
is the problem? The electromagnetic spectrum has been allocated in an
“ad hoc, piecemeal system” by the government, generally in response to
the pressures of the moment, be they commercial or military. Demand for
spectrum for wireless applications is now doubling on an annual basis,
hence the vaunted spectrum “shortage.”^[93](#ch4en93){#rch4en93}^ But,
as Meinrath notes, “most spectrum lies fallow,” as spectrum “utilization
rates are in the single digits throughout most the country. It’s not
that folks [](){#page_115}are hogging spectrum, they’re warehousing
it—mothballing it away so no one else can use
it.”^[94](#ch4en94){#rch4en94}^</span>

<span>What exists is a “false scarcity” wherein AT&T and Verizon
“continue to gobble up more and more of the spectrum capacity needed to
provide wireless service.”^[95](#ch4en95){#rch4en95}^ Matt Wood of Free
Press notes that “Verizon and AT&T have insurmountable advantages in the
current system of spectrum allocation, which allows them to outbid
everyone else—both in FCC spectrum auctions and on the ‘secondary
market’ when other licensees look to sell—and then hoard the spectrum
they have without really putting it to good use quickly
enough.”^[96](#ch4en96){#rch4en96}^ In 2011, one industry trade
publication reported that AT&T had license to \$10 billion worth of
spectrum that was lying fallow, while it lobbied to have more spectrum
diverted to it.^[97](#ch4en97){#rch4en97}^ This guarantees that no
alternative can emerge.^[98](#ch4en98){#rch4en98}^</span>

<span>In a sane society, policy debates over spectrum would concern how
best to utilize this public resource. As Peter Barnes argues, such a
policy is not necessarily “socialist”—it would invigorate businesses by
lowering their costs along with everyone else’s and by dramatically
improving service.^[99](#ch4en99){#rch4en99}^ In the United States, the
incumbents prevent such a debate, and politicians are quick to see
selling off spectrum as a way to look like “deficit hawks,” regardless
of the shortsighted nature of such a policy. At any rate, AT&T and
Verizon have propagated the notion that there is a spectrum shortage and
they need to be able to grab even more. The cartel’s claims have been
dismissed by numerous experts with no material stake in the outcome.
“Arguing that the nation could run out of spectrum is like saying it was
going to run out of a color,” David P. Reed, told the *New York Times.*
As the *Times* reported, Reed, “one of the original architects of the
Internet and a former professor of computer science and engineering at
the Massachusetts Institute of Technology, says electromagnetic spectrum
is not finite.” Technologies exist to accommodate a dramatic increase in
users, and the “shortage” can be solved by policy
making.^[100](#ch4en100){#rch4en100}^ As MacKinnon notes, “There is no
sign that Congress is serious about tackling this core problem of
monopoly and pseudo-monopoly held by many wireless and broadband
companies in many parts of the
country.”^[101](#ch4en101){#rch4en101}^</span>

<span>Elements of the FCC and the government as well as the business
community are concerned about this situation. After all, the digital
economy depends upon ubiquitous high-speed Internet, but it is hamstrung
by the [](){#page_116}cartel. One *Forbes* writer reflected the growing
concern among businesses that America badly lags behind most advanced
nations in broadband speed and prices. “This inferiority is almost
purely a result of the lack of true competition and pro-consumer
regulation in the telecom industry.”^[102](#ch4en102){#rch4en102}^ The
President’s Council of Economic Advisers issued a report in February
2012 calling for more spectrum to be auctioned to improve wireless
broadband.^[103](#ch4en103){#rch4en103}^ Later in 2012 a presidential
advisory committee, including executives from Microsoft and Google,
urged President Obama “to adopt technologies that would use radio
spectrum more efficiently.”^[104](#ch4en104){#rch4en104}^ But while some
in Washington might wish to see this new spectrum enable a credible
challenger to the ISP cartel, the effects would not be felt for five to
ten years, and there is no evidence that either political party wants to
have a head-on collision with the cartel over what the cartel regards as
a matter of life and death: maintaining its vise-like grip on Internet
access. Those closest to the action in Washington are highly skeptical
of any meaningful reform. The Obama-era FCC “has been willfully ignorant
and avoided meaningfully addressing the dearth of competition in the US
broadband industry,” one high-level activist told me in
2012.^[105](#ch4en105){#rch4en105}^ Meinrath concurred, calling the
spectrum talk “window-dressing” and stating that “a more honest
assessment would be that the FCC is actively supporting the telco
cartels.”^[106](#ch4en106){#rch4en106}^ It was striking that when FCC
chairman Julius Genachowski wanted to locate more spectrum in 2012, he
was more comfortable lobbying the Pentagon to turn over some of its
spectrum to private companies, rather than pursue the unused holdings of
the cartel.^[107](#ch4en107){#rch4en107}^ “The prospects for broadband
competition are as bad as they’ve ever been,” a leading public interest
policy analyst observed in May 2012. “In fact, they’re much worse than
when we first started banging this drum 6 years
ago.”^[108](#ch4en108){#rch4en108}^</span>

<span>There is a striking comparison here to health care, for which
Americans pay far more per capita than any other nation but get worse
service, due to the parasitic existence of the health insurance
industry. President Barack Obama said that if the United States were
starting from scratch, it would obviously make more sense (from a public
welfare and cost standpoint) to have publicly insured health care and no
private health insurance.^[109](#ch4en109){#rch4en109}^ The same logic
applies to broadband Internet access. It is worth noting that this is
how Senator Al Gore understood matters during his years in Congress,
when he championed funding for the Internet. In 1990 he argued that the
foundation for the “information superhighway” should be a public network
analogous to the interstate highway
system.^[110](#ch4en110){#rch4en110}^ Commercial interests could
[](){#page_117}use the network, much as commercial businesses use the
highways. The telecommunication companies would have a role, get
contracts, and gradually increase their role, but the government would
be in the driver’s seat, coordinate the system, and guarantee ubiquitous
access and public interest standards.^[111](#ch4en111){#rch4en111}^ That
generally uncontroversial assessment was buried under an avalanche once
Wall Street cast its eyes that way, leading Vice President Al Gore to
start singing a different tune. It has long been forgotten.</span>

<span>The parallel with the health care situation can be expanded: Just
as the health insurance companies have no interest in taking on
unhealthy customers or people from “risky” demographic groups, who might
hurt their bottom lines, the wired broadband providers have no desire to
solicit customers in poor or rural areas, where the firms find either
revenues are too low or costs are too high, or both. In a nation with as
much inequality and poverty as the United States, that can be
devastating. Wireline broadband costs nearly twice as much in the United
States as in Sweden, for example, and prices increased nearly 20 percent
from 2008 to 2010.^[112](#ch4en112){#rch4en112}^ An extensive
investigative report in 2012 revealed that as of December 2010, “40
percent of households did not have broadband connection in the home.”
Homes in wealthier neighborhoods subscribe to broadband in the 80 to 100
percent range, while impoverished households in the same city subscribe
at half that rate. The poorest states in the nation all subscribe at
under 50 percent. “Access to broadband has become critical for anyone to
keep up in American society,” the report observes. “Finding and applying
for jobs often takes place entirely online. Students receive assignments
via e-mail. Basic government services are routinely offered online.” The
digital divide thereby *accentuates* the gnawing inequality in the
United States. The “solution” for the unwired is the cell phone with
some Internet access, but as the report concludes, “a smart phone is no
substitute for a home computer with a wire-line connection, at least not
today.”^[113](#ch4en113){#rch4en113}^ And as we will see shortly,
probably not tomorrow either.^[114](#ch4en114){#rch4en114}^</span>

<span>There are currently two great policy battles in the United States
that may reduce the damage caused by the ISP cartel. First is the
movement for local communities to establish their own broadband
networks, “just as local governments a century ago wanted their
communities to have affordable access to reliable electric power.”
Countless cities have been ignored or feel gouged by the cartel, and
more than 150 of them throughout the nation have built their own
networks. The private ISPs tend to be unwilling “to invest in next
[](){#page_118}generation broadband networks except in the most
lucrative markets.”^[115](#ch4en115){#rch4en115}^ To many of the
communities left out in the cold, this is regarded as a matter of life
and death. A 2012 study by the New America Foundation demonstrates that
American universities are well positioned to be the “primary anchor
institutions” that provide “robust physical infrastructure that can be
leveraged to provide high-speed Internet access into
communities.”^[116](#ch4en116){#rch4en116}^</span>

<span>The cartel has responded to community broadband exactly as the
health insurance industry did to the idea of a viable “public option” in
the 2009–10 health care debates. It has deployed its vast resources and
lobbying armada in what can only be described as withering attacks in
nearly all states to make municipal broadband networks all but
impossible, if not illegal. By 2012 nineteen states had passed such
laws.^[117](#ch4en117){#rch4en117}^ In North Carolina, for example,
which passed a law restricting local governments from building broadband
networks in 2011, the giant telecom companies and their trade
associations gave nearly \$1.8 million to North Carolina state
candidates between 2006–11.^[118](#ch4en118){#rch4en118}^ At the same
time, that the total is *only* 19 states reflects the fact that popular
campaigns to protect the right for communities to establish broadband
networks have been able to thwart, or at least stall, the cartel. One
*Economist* writer based in Atlanta raved about Chattanooga’s
municipally owned high-speed broadband network: “Meanwhile, here in
Atlanta, a region of over 4m people, I’m stuck with mediocre Comcast
service that conks out every time I look at it
funny.”^[119](#ch4en119){#rch4en119}^ Once people experience community
or municipal broadband, as in Santa Monica, California, it is much
harder for the cartel’s battalions of lobbyists and trunkloads of
campaign donations to take it away.^[120](#ch4en120){#rch4en120}^ In
both 2005 and 2007, a bipartisan group of senators including John Kerry
and John McCain introduced legislation that would have stopped states
from blocking their cities and towns from building their own broadband
networks. The cartel was able to derail the legislation then, but that
approach remains the best immediate solution to the problem.</span>

<span>The second policy battle is over Net neutrality. This is a
requirement that ISPs not discriminate among users, following the old
common carrier requirements on the telephone monopoly. Technically, this
meant ISPs “could not discriminate against packets moving across their
networks.”^[121](#ch4en121){#rch4en121}^ In the 1990s many Americans
assumed the Internet was a magical platform that let everyone have an
equal right to speak, thanks to the technology. In fact, the democratic
genius of the Internet was the *regulation* that prohibited ISPs from
discriminating among legal Internet activities, so a punk rock
[](){#page_119}or vegan website got the same treatment as Microsoft’s
website. The ISPs hated this regulation; if they could discriminate
among users, they could effectively privatize the Internet and make it
like cable television. For cartel members, it is less about their desire
to censor dissident speech than their desire to extort extra fees from
commercial players on their networks. AT&T CEO Ed Whitacre proclaimed as
much in an interview with *BusinessWeek* in 2005. What Internet websites
and applications “would like is to use my pipes for free, but I ain’t
going to let them do that.”^[122](#ch4en122){#rch4en122}^ Users did and
do pay the ISPs to use their networks; what the cartel wanted was the
right to discriminate, charge big users more, and collect additional
undeserved “rents.” In a world without Net neutrality, the potential for
increased ISP profits was and is mind-boggling.</span>

<span>Business analysts and publications like *The Economist* argued
that the obvious market solution to the problem was more competition. If
consumers had choices, no one would ever sign up to an ISP that censored
websites or discriminated among them. But the cartel made that about as
realistic politically as passing a constitutional amendment outlawing
heterosexuality, so the campaign for the maintenance of Net neutrality
became the crucial battle to prevent the elimination of the Internet as
an open public realm.</span>

<span>Led by Free Press, an enormous campaign mushroomed around 2005–6
to maintain Net neutrality. There was support on the political grounds
that it was singularly dangerous to permit a small handful of private
concerns to have a censor’s power over what had become the primary
marketplace of ideas. “In many countries,” MacKinnon points out, “a lack
of net neutrality makes censorship—whether by companies, government, or
some mix of the two—much easier to implement and much less publicly
visible, let alone accountable.”^[123](#ch4en123){#rch4en123}^ There was
also support for Net neutrality from the business community, especially
from powerful firms like Google, which did not want to be shaken down by
ISPs in order to get on their networks. In 2008 a frustrated Vint Cerf,
by then a Google executive, asked if it might not be better if the
Internet data-pipe infrastructure were “owned and maintained by the
government, just like the highways.”^[124](#ch4en124){#rch4en124}^
Candidate and later president Barack Obama loudly announced that he
would “take a back seat to no one in my commitment to net neutrality,”
and that it would be the centerpiece of his communications policy
regime.^[125](#ch4en125){#rch4en125}^</span>

<span>The formal Net neutrality policy that the FCC approved in December
2010 maintained effective neutrality for the wired ISPs but effectively
[](){#page_120}abandoned it for the wireless ISPs, where much of the
action was moving. “It’s the internet vs. the schminternet,” as Jeff
Jarvis put it.^[126](#ch4en126){#rch4en126}^ The actual policy closely
approximated the agreement Google and Verizon had privately reached on
Net neutrality in August 2010.^[127](#ch4en127){#rch4en127}^ That
meeting took on the air of the meeting of the five families in *The
Godfather* to divvy up the illegal-drug trade in New York City. Jarvis
termed it a “devil’s pact,” and it was a textbook example of how
communication policy is made.^[128](#ch4en128){#rch4en128}^ As of 2013,
mobile phones have overtaken PCs as the most common way to access the
Web, and that will be an increasingly proprietary
world.^[129](#ch4en129){#rch4en129}^ Nor will its influence end there.
“The closed smartphone architecture is the canary in the coalmine for
all of consumer computing,” Harvard’s Jonathan Zittrain says, concluding
that “the PC is dead.”^[130](#ch4en130){#rch4en130}^</span>

<span>Free Press, the New America Foundation, and most public interest
advocates regarded the policy as a failure, an abandonment of the Obama
administration’s oft-stated position. The fingerprints of the wireless
ISPs were all over it, and it was easy to see the fear the Obama
administration had about antagonizing such a powerful and well-heeled
lobby, especially with a billion-dollar election around the corner.
Republicans opposed *any* Net neutrality regulations. The ISPs sensed
weakness and pushed ahead in the courts to have even the FCC policy
rejected, if not the very notion of Net neutrality. That is where the
matter stands at this writing. If this stops being an issue among
politicians, it will be because the cartel has won.</span>

## The *Titanic* Sails Again? {#the-titanic-sails-again .h3}

<span>From the 1970s to the end of the 1990s, in a dramatic
transformation, the U.S. media system came to be dominated by a handful
of entertainment conglomerates—Time Warner, News Corporation, Viacom,
Disney, General Electric, and one or two others. After a wave of ever
larger mergers and acquisitions, these companies owned all the major
television networks, many of the largest-market television stations,
hundreds of radio stations, all the major film studios, many cable TV
systems, most of the cable TV channels, and much of the music recording
industry. These firms also had large stakes in magazine and book
publishing, and a few newspapers. Some of them—like Sony, General
Electric, and Disney—had extensive holdings outside traditional media.
In just fifteen years, between 1984 and the end of [](){#page_121}the
1990s, the share of the five largest conglomerates in total media-sector
revenues doubled to around 26 percent.^[131](#ch4en131){#rch4en131}^
That may not sound like much by recent American standards, but the media
sector includes as many as ten fairly distinct industries that
traditionally often had their own unique firms. Several of these
industries, like book publishing, had been rather competitive in the not
too distant past.^[132](#ch4en132){#rch4en132}^ This concentration would
be somewhat akin to having five firms control 26 percent of the revenues
of all branches of food production, grocery stores, and
restaurants.</span>

<span>By 2000 there was also a second tier of a dozen or so lesser
conglomerates that tended to be newspaper-based empires—like Gannett,
the Tribune Company, Cox, the Washington Post and the New York
Times—that had interests in television stations and publishing. These
firms were pikers compared to the megaconglomerates, and they did not
grow as quickly, but they were an important component of the media
system, especially for journalism. Once one got past the first two dozen
or so firms, the companies that remained were much smaller and less
powerful. It was a quick trip from the redwood forest to the weeds. By
the end of the 1990s, these two dozen largest firms were made up of what
were once several hundred independent media companies as recently as the
1970s.^[133](#ch4en133){#rch4en133}^</span>

<span>By 2000 there were eighteen media and advertising firms that
qualified for the Fortune 500, compared to eight in 1970; eight
conglomerates qualified for the top 150, compared to two such firms in
1970.^[134](#ch4en134){#rch4en134}^ Media concentration seems to many to
violate the principle of open and diverse media required for a
democratic culture. To the extent it was seen as affecting journalism,
it became a growing concern.^[135](#ch4en135){#rch4en135}^ These firms
had gotten larger for the same reasons other capitalist firms get
larger: bigness reduces risk and increases profitability, everything
else being equal.^[136](#ch4en136){#rch4en136}^ It’s important to
remember that conglomeration required a number of significant changes in
federal laws and regulations—largely because most of these firms traded
in radio and TV stations or cable TV systems, the licenses for which had
strict ownership regulations to prevent monopoly—but media firms proved
to be highly skilled at getting their way in Washington.</span>

<span>By the mid-1990s, for media moguls like Rupert Murdoch and
Disney’s Michael Eisner, it seemed the world was their oyster. But for
all the depth and breadth of the empires they had constructed and
despite all their political influence, the Internet seemingly posed a
threat to their very existence. As Jaron Lanier put it, “the old-media
empires were put on a path of [](){#page_122}predictable
obsolescence.”^[137](#ch4en137){#rch4en137}^ The Internet appeared to
pose this threat for three reasons. First, it opened the possibility of
making it much easier for new players to enter media markets. As the
Internet became the dominant platform, prospective entrants would no
longer need major capital to get a broadcasting license or buy an
existing film studio. With barriers to entry eliminated, the digital era
might make it possible for another giant with an enormous bankroll, say
a Microsoft or an AT&T, to successfully use the Internet as a platform
to get in the media game, whereas it would have been unthinkable
otherwise.</span>

<span>Like the telephone giants, the media conglomerates and their
lobbyists argued that ownership regulations were no longer relevant to
their industries and should be abolished. Why? Because as all media
shifted to digital formats, the traditional distinctions among media
sectors would disappear—a process called convergence—with a consequent
tidal wave of digital competition. Media giants needed to be allowed to
get much, much larger to withstand the impending competitive war.
Otherwise they would likely die, and it would be unfair to force them to
compete in the digital race for survival with their shoelaces tied
together. Partially as a result of this argumentation, for example, the
1996 Telecommunications Act greatly relaxed radio station ownership
rules, leading to massive consolidation in the next three years. This
notion of new digital competition for the media giants was embraced by
many digital activists in the 1990s, who thought the big media
corporations were getting their just deserts. They would all soon be
submerged by the Internet, with its unlimited number of
websites.^[138](#ch4en138){#rch4en138}^ All sorts of newcomers could
enter what had been a restricted field, and if they could locate a
following, they would be able to generate sufficient revenues to make a
go of it. Jaron Lanier remembers the idealistic conviction that a
digital utopia was around the corner in a cultural system soon to be
liberated from the commercial
monopolists.^[139](#ch4en139){#rch4en139}^</span>

<span>The second threat to the media conglomerates was the difficulty of
getting customers to pay for media content online, because it was so
ridiculously easy to copy and distribute perfect digital copies of
music, films, TV shows, and the like at no charge. The Internet
magnified the inherent problem with markets for cultural production and,
in effect, made the commercial media system impossible. Copyright was
finally overmatched. One could imagine the end of the recording
industry, Hollywood, television, and book publishing. The prospective
demise of the commercial media system could [](){#page_123}have
generated public study and debate over how best to encourage cultural
production and have cultural workers effectively compensated, that is,
what policies could replace copyright in the digital era. But the media
conglomerates had no interest in such a discussion, so it never
occurred. Their business model is built on the dramatic expansion of
copyright, and without it there would not be much of an industry.</span>

<span>Commercial media dealt with the public good of terrestrial radio
and TV broadcasting by turning to advertising as the source of revenues.
In theory this could have been part of a solution to their dilemma with
the Internet. But all evidence suggested that people would never sit
through thirty-second spots on their computer; they would go to another
site. Advertising, which made television one of the great engines of
profit for generations, was in the digital crosshairs. This was the
third great threat posed by the Internet to the media
conglomerates.</span>

<span>In combination, the three threats would take away audience with
all sorts of new free offerings on the infinite World Wide Web. It would
make “piracy” so rampant that there would be very little commercial
incentive to produce content. Scarcity would no longer exist, so there
could be no basis for markets.^[140](#ch4en140){#rch4en140}^ And it
would be impossible for advertising to bankroll Internet media, because
customers would not tolerate it, and in the land of digital plenty,
customers would have an infinite number of choices. This was enough to
make any media CEO want to upload a résumé and look for a new field to
conquer.</span>

<span>The Internet explosion of the 1990s scared the dickens out of the
media giants, and they responded by doing what was second nature to
them: buying the competition. They frantically spent billions of dollars
gobbling up digital ventures so they would not be outflanked by any
digital media upstarts. They would buy up everything they could on the
Internet so no matter how it developed, they would own the damn thing.
That experience is now chalked up as among the most insane flights of
fancy in business history and an unmitigated disaster for the media
giants, who acted as if they had only a matter of months until the
Internet destroyed them. Some of the digital ventures they invested in
were laughably implausible. The nadir came in January 2000 when the
AOL–Time Warner merger was announced; AOL was the dominant partner in
the deal even though it had just a smidgen of Time Warner’s assets,
sales, or profits. What AOL had was a mother lode of world-class hype.
It soon became clear that once the Internet shifted from dial-up
[](){#page_124}to broadband, AOL had no business model and was next to
worthless.^[141](#ch4en141){#rch4en141}^ How could Time Warner, the
mightiest of the mighty media conglomerates, have missed something so
elementary?</span>

<span>Despite the hit to corporate balance sheets due to the Internet
bubble, in 2000 commercial media markets remained lucrative, and the
immediate future looked just fine, especially for the Hollywood-based
conglomerates. It would take widespread broadband to make the threats
genuine. In the meantime, the media giants had oodles of cash and
considerable lobbying power to try to redirect digital media before
broadband hit full-force. They had also learned an important lesson by
then: if the media giants couldn’t find a successful business model for
media online, no one else would either. To the extent that anyone could
make a profit producing online media content, the media giants, with
their vast collection of content and resources, had dramatic advantages
over everyone else. This changed the nature of the struggle, lengthening
their shelf life by decades, and, insofar as content had value, giving
them considerable leverage.</span>

<span>The corporate media sector has spent much of the past fifteen
years doing everything in its immense power to limit the openness and
egalitarianism of the Internet. Its survival and prosperity hinge upon
making the system as closed and proprietary as possible, encouraging
corporate and state surreptitious monitoring of Internet users and
opening the floodgates of commercialism.^[142](#ch4en142){#rch4en142}^
By 2012, media firms were holding their own. Although asset-shuffling
and deal making continued—Comcast, for example, bought a controlling
interest in NBC Universal from General Electric in 2011—the degree of
concentration has plateaued at around 2000 levels. The largest media
firms maintained their grip, with about the same number ranking in
*Fortune*’s top 150 and top 500 in 2011 as in
2000.^[143](#ch4en143){#rch4en143}^</span>

<span>The most important campaign has been to extend the scope and
length of copyright and make enforcement as sweeping and penalties as
onerous as possible. The copyright lobby has dominated congressional and
regulatory deliberations. As MacKinnon put it, “The need to protect
intellectual property has become a higher priority for many elected
officials than due process—the presumption that a person is innocent
until proven guilty.”^[144](#ch4en144){#rch4en144}^ Another major prong
of this power grab was the development of digital rights management
(DRM) technologies that imposed artificial limitations on the
functionalities of digital devices and
software.^[145](#ch4en145){#rch4en145}^</span>

<span>From 1998’s Digital Millennium Copyright Act to the failed attempt
[](){#page_125}in 2011–12 to pass the Stop Online Piracy Act (SOPA),
copyright law has moved from the sleepy backwater of law school
curricula—providing the rules of the road for a long-established
commercial system—to the forefront as an offensive weapon for shaping
our media and communication systems. SOPA would have given the
government the power to shut down entire domains, with very little
transparency and no meaningful repercussions for erroneous actions. It
would have extended and legitimated the already extensive activities of
ISPs to censor websites accused of “piracy,” with minimal standards for
due process and fairness. The measure, according to *Wired* magazine,
also would have paved “the way for private rights holders to easily cut
off advertising and banking transactions to what the bill’s backers call
‘rogue websites,’ without court intervention.” As Google’s Sergey Brin
told *The Guardian* in an interview, SOPA “would have led to the US
using the same technology and approach it criticized China and Iran for
using.”^[146](#ch4en146){#rch4en146}^ Representative Zoe Lofgren (D-CA)
may have spoken with only a tad of hyperbole when she stated that its
passage “would mean the end of the Internet as we know
it.”^[147](#ch4en147){#rch4en147}^ The details were so frightening that
*Wikipedia* closed its website for a full day in protest. Although
defeated in 2012, the issue will return to Congress in coming years,
perhaps in sheep’s clothing.</span>

<span>Another measure of the power of the copyright lobby is how the
federal government has made copyright enforcement the highest possible
priority in trade deals and has pressured other governments to adopt
U.S.-style laws and enforcement—to the point where a casual observer
might think U.S. officials were on the media industry’s
payroll.^[148](#ch4en148){#rch4en148}^ The United States has led the
fight for the Anti-Counterfeiting Trade Agreement (ACTA), which as of
2012 had over thirty signatories and is in the process of being
ratified. The United States kept top secret its core provisions until
they were released by WikiLeaks in 2008. ACTA would empower governments
to cut off Internet access for alleged copyright violators without due
process and to remove content without proof of any violation. After
worldwide protests, these stipulations were watered down grudgingly, but
the concerns of copyright holders are clearly the point of the exercise
and a higher priority than human rights. In July 2012 the European
Parliament rejected ACTA, largely due to the public outcry over the
onerous copyright extensions, which pretty much killed it. Attention
then turned to the Trans-Pacific Partnership treaty being negotiated
between the United States and Pacific Rim nations. Feld saw in those
negotiations indications that the U.S. trade representative—after
[](){#page_126}the publicity beating over ACTA—was beginning to
recognize that a softer hand, and more acceptance of such matters as
fair use and the public domain, might be necessary to gain passage of
trade deals.^[149](#ch4en149){#rch4en149}^ But some people close to the
action do not expect meaningful changes in the U.S. position on
copyright either internationally or domestically. “These efforts are
absolutely independent of public input,” James Losey told
me.^[150](#ch4en150){#rch4en150}^ Meinrath is dubious about the efforts
of policy makers to appease concerned citizens about new copyright laws
and treaties: “All multi-stakeholder efforts I’ve heard about have been
more towards the PR BS side of the spectrum than anything
meaningful.”^[151](#ch4en151){#rch4en151}^</span>

<span>Note well, as MacKinnon chronicles, the lessons from China and
Russia are that those governments routinely use copyright enforcement as
a politically convenient cover for cracking down on
dissent.^[152](#ch4en152){#rch4en152}^ There is also a synergy of
interests between the commercial forces that want to monitor people
surreptitiously online to better sell them to advertisers and the
copyright holders who want to monitor people online to see who might be
using their material without
permission.^[153](#ch4en153){#rch4en153}^</span>

<span>The irony is this: research demonstrates that while aggressively
enforcing onerous copyright laws can quash dissent, because of the
technology, that approach is ineffective at reducing the supply of
“pirated” material online.^[154](#ch4en154){#rch4en154}^ “In the long
run,” as David Friedman puts it, “simply enforcing existing law is not
going to be an option.”^[155](#ch4en155){#rch4en155}^ Scholars like Pat
Aufderheide and Peter Jaszi propose smart reforms, while others, like
Yochai Benkler, Lawrence Lessig, and Friedman, demonstrate that there
may be ways to make cultural production compatible with the
Internet.^[156](#ch4en156){#rch4en156}^ “I think we are at a point where
we are asking whether you really need a film industry for a film to be
made or a music industry to make music,” Kickstarter co-founder Yancey
Strickler put it in 2012.^[157](#ch4en157){#rch4en157}^ The problem, of
course, is that alternative approaches are not compatible with the media
giants remaining enormous and enormously profitable.</span>

<span>Independent of the legislative front, in 2012 the media giants
proceeded to work out private arrangements to enforce copyright to their
satisfaction with the telecom cartel and Internet giants such as Google.
The Center for Copyright Information was formed so that the ISPs would
police their networks for “pirated” material. After six warnings, an
infringing user would have the material significantly downgraded or
removed. The exact manner in which the system would work has been
obscure, as have been the rights of [](){#page_127}the accused. The
potentially explosive plan was to go into effect in July 2012 but was
delayed to work out the details. As one observer noted, the “ISPs are
not the most popular companies, and playing policeman for Hollywood will
not make that better at all.”^[158](#ch4en158){#rch4en158}^</span>

<span>The media firms did get Google to agree to alter its search
algorithms to favor copyrighted material in August 2012. Google
preferred a private agreement to a bill like SOPA that could make more
onerous and costly demands on it to police copyright. Websites that were
repeatedly challenged on copyright grounds would be listed so deep into
Google searches that they would effectively no longer exist. The
concerns again are that the process is opaque and, as a public interest
advocate told Talking Points Memo, “appears to incentive copyright
holders to file many removal requests.” Indeed, Google received
copyright removal requests for over 4.3 million Web addresses within a
thirty-day period during summer 2012, more than it received in all of
2009.^[159](#ch4en159){#rch4en159}^ “Google has set up a system that may
be abused by bad faith actors who want to suppress their rivals and
competitors,” one public interest lawyer
stated.^[160](#ch4en160){#rch4en160}^ As Free Press’s Wood put it, “any
number of deals that studios, ISPs, and search engine companies are
ready, willing, and able to cut with each other” can threaten the open
Internet just as much as “bad
legislation.”^[161](#ch4en161){#rch4en161}^ Should private
self-interested monopolies really be making secret policy that directs
the future of cyberspace, without public awareness or
participation?</span>

<span>The most significant development in the media giants’ battle to
stem “piracy” has been the emergence of Apple iTunes, Netflix, legal
streaming systems, and e-books as ways to sell content online. Forty
percent of Americans who said they had illegally downloaded videos said
the legal proprietary streaming services made them less likely to do
so.^[162](#ch4en162){#rch4en162}^ By 2010 nearly a third of record
companies’ global revenues came from digital distribution, and the
proportion was rising quickly. By 2011, Amazon’s sales of e-books
exceeded its sales of printed books.^[163](#ch4en163){#rch4en163}^ One
major publisher saw the percentage of its revenues from e-book sales
increase from 11 percent in 2010 to 36 percent by the end of 2011, and
this is the trend industrywide.^[164](#ch4en164){#rch4en164}^ For the
public, these legal alternatives are mixed blessings, because they are
closed, proprietary systems devised to establish and maintain artificial
scarcity, so as to give immense power to private monopolies. The problem
for media giants is that they give firms like Apple and Amazon a great
deal of power over pricing. The 2012 battle among Amazon, Apple, and
book publishers over the [](){#page_128}pricing of e-books, for example,
pointed to a future in which publishers may not be necessary, except in
the sales of their existing copyrighted material. Hence the media giants
are doubling down on their campaign for onerous copyright laws, extreme
enforcement, and draconian
penalties.^[165](#ch4en165){#rch4en165}^</span>

<span>Media conglomerate bottom lines notwithstanding, it remains
uncertain if these proprietary systems can effectively generate a
revenue base to sustain a large system of original cultural production.
Lanier, for one, has abandoned his utopianism for what he terms
empiricism. “To my shock, I have trouble finding a handful of musicians”
(besides artists like Ani DiFranco) who have bypassed the corporate
system and made a go of it online. “After ten years of seeing many, many
people try,” he concluded in his assessment of the Internet and culture,
“I fear that it just won’t work for the vast majority of journalists,
musicians, artists, and filmmakers who are staring into career oblivion
because of our failed digital
idealism.”^[166](#ch4en166){#rch4en166}^</span>

<span>The one truly hopeful sign for the media conglomerates is the
extraordinary durability of television. In 2012 some \$60 billion was
spent on TV advertising, mostly flowing to the large conglomerates;
online video received only \$3 billion in advertising, although that was
a 55 percent increase over 2011.^[167](#ch4en167){#rch4en167}^ What is
developing is a merger of sorts between digital television and the
Internet. Online streaming currently reaches one third of all TV viewers
on a weekly basis.^[168](#ch4en168){#rch4en168}^ “Four to five hours per
day is what Americans spend consuming video,” Hulu CEO Jason Kilar
noted, and the battle is on to see who controls that four to five hours,
whether it is on a television, a laptop, or a
smartphone.^[169](#ch4en169){#rch4en169}^ According to Cisco Systems,
video accounted for 40 percent of Internet traffic in 2012 and will
account for around 60 percent in 2015.^[170](#ch4en170){#rch4en170}^ As
this is an advertising-supported area for the most part, the convergence
is accelerating as digital television becomes “addressable” like the
Internet. “Contemporary activities suggest,” Joseph Turow writes, “that
eventually there will be little difference between the ‘internet’ and
‘television’ in terms of advertisers’ approach to people and their
data.”^[171](#ch4en171){#rch4en171}^ The media conglomerates are working
diligently to become the main content providers and to have control over
the channels, whatever the precise
medium.^[172](#ch4en172){#rch4en172}^</span>

<span>Firms like Apple, Amazon, and Google—not to mention the ISP
cartel—are joining the battle to control video consumption, and are all
scurrying to expedite and capitalize upon this marriage of digital TV
and the Internet. As the *New York Times* puts it, these “battles are
part of the larger war for three screens: smartphones, tablets, and
televisions.” A 2012 Pew survey [](){#page_129}determined that 52
percent of all adult cell phone users “incorporate their mobile devices
into their television watching
experiences.”^[173](#ch4en173){#rch4en173}^ Google’s YouTube, for
example, is launching one hundred TV-style advertising-supported
Internet channels.^[174](#ch4en174){#rch4en174}^ “We want to make all
your screens work together in a unique and seamless way,” a Microsoft
executive explains. The growing consensus is that “whichever company
ends up owning the living room, where most content is consumed, could
own the entire sphere.”^[175](#ch4en175){#rch4en175}^</span>

<span>It is unclear where this process will end up—and what the division
of power will be among the corporate sectors—except that it is mostly
being driven by advertising, in a form radically different from how it
has been traditionally understood. The smart money says not to bet
against the new digital giants as they square off with the media
conglomerates for the largest slice of the
pie.^[176](#ch4en176){#rch4en176}^ Let’s find out why.</span>

# References

<span>[1](#rch4en1){#ch4en1}. Transcript, “Special Event: George W. Bush
Addresses Rally in Appleton, Wisconsin,” Oct. 28, 2000,
<http://archives.cnn.com/TRANSCRIPTS/0010/28/se.02.html>.</span>

<span>[2](#rch4en2){#ch4en2}. Tom Streeter notes that “it seems
plausible that the ‘Gore said he invented the Internet’ quip did at
least as much damage to Gore’s final vote count as Ralph Nader.” See
Thomas Streeter, *The Net Effect: Romanticism, Capitalism, and the
Internet* (New York: New York University Press, 2011), 114–15.</span>

<span>[3](#rch4en3){#ch4en3}. Transcript, “Vice President Gore on CNN’s
Late Edition,” Mar. 9, 1999,
[www.cnn.com/ALLPOLITICS/stories/1999/03/09/president.2000/transcript.gore/index.html](http://www.cnn.com/ALLPOLITICS/stories/1999/03/09/president.2000/transcript.gore/index.html).</span>

<span>[4](#rch4en4){#ch4en4}. *Matrix News* 9, no. 4 (Apr. 1999),
<http://web.archive.org/web/20000125065813/>
<http://www.mids.org/mn/904/vcerf.html>.</span>

<span>[5](#rch4en5){#ch4en5}. Some conservatives are obsessed with
discrediting or at least trivializing the idea that the government and
the military played the foundational role in establishing the Internet,
for explicitly political reasons. As former *Wall Street Journal*
publisher Gordon Crovitz wrote on the *Journal*’s website, if people
understand the government could create the Internet, it could be
successfully “cited to justify big government.” L. Gordon Crovitz, “Who
Really Invented the Internet?” *Wall Street Journal* online, July 22,
2012,
[online.wsj.com/article/SB10000872396390444464304577539063008406518.html](http://online.wsj.com/article/SB10000872396390444464304577539063008406518.html).
The “argument” was immediately rebuffed for the ideological exercise it
was by no less a free-market libertarian than economist Dan Mitchell.
The government role in establishing the Internet is “well-established,
easily discovered truth.” Mitchell explained that this is “how
innovation actually happens—sometimes: the government pours money and
engineering talent into a research project, the results of which are
then unleashed into the marketplace, where they are exploited and
further developed by commercial interests.” See Dan Mitchell, “Untruths
at the Origins of the Internet,” CNN Money, July 24, 2012,
[tech.fortune.cnn.com/2012/07/24/untruths-at-the-origins-of-the-internet](http://tech.fortune.cnn.com/2012/07/24/untruths-at-the-origins-of-the-internet).</span>

<span>[6](#rch4en6){#ch4en6}. Peter Nowak, *Sex, Bombs and Burgers: How
War, Pornography, and Fast Food Have Shaped Modern Technology*
(Guilford, CT: Lyons Press, 2011), 203.</span>

<span>[7](#rch4en7){#ch4en7}. Sascha D. Meinrath, James W. Losey, and
Victor W. Pickard, “Digital Feudalism: Enclosures and Erasures from
Digital Rights Management to the Digital Divide,” *CommLaw Conspectus*,
19, no. 2 (2011): 459; and Tim Wu, *The Master Switch: The Rise and Fall
of Information Empires* (New York: Knopf, 2010).</span>

<span>[8](#rch4en8){#ch4en8}. John Naughton, *What You Really Need to
Know About the Internet: From Gutenberg to Zuckerberg* (London: Quercus,
2012), 45–46.</span>

<span>[9](#rch4en9){#ch4en9}. Susan Landau, *Surveillance or Security?
The Risks Posed by New Wiretapping Technologies* (Cambridge, MA: MIT
Press, 2010), 18.</span>

<span>[10](#rch4en10){#ch4en10}. Nathan Newman, *Net Loss: Internet
Prophets, Private Profits, and the Costs to Community* (University Park,
PA: Penn State University Press, 2002), 52–53.</span>

<span>[11](#rch4en11){#ch4en11}. James Curran, “Rethinking Internet
History,” in James Curran, Natalie Fenton, and Des Freedman,
*Misunderstanding the Internet* (London: Routledge, 2012), 37.</span>

<span>[](){#page_251}[12](#rch4en12){#ch4en12}. Nowak, *Sex, Bombs and
Burgers*, 9.</span>

<span>[13](#rch4en13){#ch4en13}. Newman, *Net Loss*, 51.</span>

<span>[14](#rch4en14){#ch4en14}. Joseph Stiglitz writes that when he was
head of the Council of Economic Advisers, the CEA determined the
“average social return on government R&D” to be “well in excess of 50
percent,” far higher than returns on private sector R&D. See Joseph E.
Stiglitz, *The Price of Inequality: How Today’s Divided Society
Endangers Our Future* (New York: W.W. Norton, 2012), 174.</span>

<span>[15](#rch4en15){#ch4en15}. Nowak, *Sex, Bombs and Burgers*,
11.</span>

<span>[16](#rch4en16){#ch4en16}. Newman, *Net Loss*, 26.</span>

<span>[17](#rch4en17){#ch4en17}. Linda McQuaig and Neil Brooks,
*Billionaires’ Ball: Gluttony and Hubris in an Age of Epic Inequality*
(Boston: Beacon Press, 2012), 77, 83.</span>

<span>[18](#rch4en18){#ch4en18}. Newman, *Net Loss*, 21.</span>

<span>[19](#rch4en19){#ch4en19}. Nowak, *Sex, Bombs and Burgers*, 11,
12.</span>

<span>[20](#rch4en20){#ch4en20}. Chris Anderson, “Here Come the Drones,”
*Wired*, July 2012, 107.</span>

<span>[21](#rch4en21){#ch4en21}. E-mail from Sascha Meinrath to author,
Jan. 6, 2011.</span>

<span>[22](#rch4en22){#ch4en22}. Newman, *Net Loss*, 57.</span>

<span>[23](#rch4en23){#ch4en23}. Kenneth David Nichols, *The Road to
Trinity: A Personal Account of How America’s Nuclear Policies Were Made*
(New York: William Morrow, 1987), 34–35.</span>

<span>[24](#rch4en24){#ch4en24}. Wu, *Master Switch*, 276.</span>

<span>[25](#rch4en25){#ch4en25}. Eden Medina, *Cybernetic
Revolutionaries: Technology and Politics in Allende’s Chile* (Cambridge,
MA: MIT Press, 2011).</span>

<span>[26](#rch4en26){#ch4en26}. Richard Adler, *Updating Rules of the
Digital Road: Privacy, Security, Intellectual Property* (Washington, DC:
Aspen Institute, 2012), 4.</span>

<span>[27](#rch4en27){#ch4en27}. Rebecca MacKinnon, *Consent of the
Networked: The Worldwide Struggle for Internet Freedom* (New York: Basic
Books, 2012), 18–19.</span>

<span>[28](#rch4en28){#ch4en28}. Heather Brooke, *The Revolution Will Be
Digitised: Dispatches from the Information War* (London: Heinemann,
2011), 24. See also Johan Soderberg, *Hacking Capitalism: The Free and
Open Source Software Movement* (New York: Routledge, 2008).</span>

<span>[29](#rch4en29){#ch4en29}. Naughton, *What You Really Need to Know
About the Internet*, 82.</span>

<span>[30](#rch4en30){#ch4en30}. Joseph Turow, *The Daily You: How the
New Advertising Industry Is Defining Your Identity and Your Worth* (New
Haven, CT: Yale University Press, 2011), 38.</span>

<span>[31](#rch4en31){#ch4en31}. Ibid., 40.</span>

<span>[32](#rch4en32){#ch4en32}. Quotation from Eli Pariser, *The Filter
Bubble: What the Internet Is Hiding from You* (New York: Penguin, 2011),
31.</span>

<span>[33](#rch4en33){#ch4en33}. Newman, *Net Loss*, 57.</span>

<span>[34](#rch4en34){#ch4en34}. Dan Schiller, *Telematics and
Government* (Norwood, NJ: Ablex, 1982), 210–14.</span>

<span>[35](#rch4en35){#ch4en35}. Ryan Ellis, currently a Stanford
postdoctoral fellow, is conducting historical research on this topic.
See Ryan Ellis, “Binding the Nation Together? Postal Policy in the Era
of Competition,” International Communication Association Conference, San
Francisco, May 2007, 57–65.</span>

<span>[36](#rch4en36){#ch4en36}. Nowak, *Sex, Bombs and Burgers*,
206.</span>

<span>[37](#rch4en37){#ch4en37}. Richard Adler, *Updating Rules of the
Digital Road: Privacy, Security, Intellectual Property* (Washington, DC:
Aspen Institute, 2012), 4.</span>

<span>[38](#rch4en38){#ch4en38}. Curran, “Rethinking Internet History,”
45.</span>

<span>[39](#rch4en39){#ch4en39}. Tim Berners-Lee, *Weaving the Web* (New
York: HarperCollins, 1999), 197–98.</span>

<span>[40](#rch4en40){#ch4en40}. Project Censored,
<http://www.projectcensored.org/top-stories/articles/category/top-stories/top-25-of-1996/page/3>.</span>

<span>[](){#page_252}[41](#rch4en41){#ch4en41}. This story is
brilliantly told in Fred Turner, *From Counterculture to Cyberculture*
(Chicago: University of Chicago Press, 2006).</span>

<span>[42](#rch4en42){#ch4en42}. Siva Vaidhyanathan develops this point
with the concept of “public failure.” See Siva Vaidhyanathan, *The
Googlization of Everything (and Why We Should Worry*) (Berkeley:
University of California Press, 2011), 39–44.</span>

<span>[43](#rch4en43){#ch4en43}. See Ha-Joon Chang, *23 Things They
Don’t Tell You About Capitalism* (New York: Bloomsbury Press, 2010), for
a superb debunking of this point and the other ideological ballast
underlying neoliberal economics.</span>

<span>[44](#rch4en44){#ch4en44}. Matthew Crain, “The Revolution Will Be
Commercialized: Finance, Public Policy, and the Construction of Internet
Advertising in the 1990s,” PhD dissertation, University of Illinois at
Urbana-Champaign, 2013.</span>

<span>[45](#rch4en45){#ch4en45}. S. Derek Turner, “The Internet,” in
*Changing Media: Public Interest Policies for the Digital Age*
(Washington, DC: Free Press, 2009), 12,
[www.freepress.net/files/changing\_media.pdf](http://www.freepress.net/files/changing_media.pdf).</span>

<span>[46](#rch4en46){#ch4en46}. Dan Schiller, *Digital Capitalism:
Networking the Global Market System* (Cambridge, MA: MIT Press, 1999),
128.</span>

<span>[47](#rch4en47){#ch4en47}. The celebratory and largely fatuous
coverage of the Internet continues in what remains of the news media to
this day. In a 2012 report for the *Columbia Journalism Review*, Michael
Massing noted that “reporters breathlessly chronicle the fortunes,
mansions, and attire of the digerati.” Yet the American press, “while
writing endlessly about the Internet, has failed to examine some
important questions about it.” Specifically, “journalists tend to
celebrate these moguls for their savvy and cool rather than examine the
enormous wealth they’ve amassed and the political and economic ends to
which they put it.” I would add that the crucial policy debates that
determine the direction of the Internet fare no better. Michael Massing,
“The Media’s Internet Infatuation,” *Columbia Journalism Review* online,
Aug. 15, 2012,
[cjr.org/the\_kicker/internet\_infatuation.php](http://cjr.org/the_kicker/internet_infatuation.php).</span>

<span>[48](#rch4en48){#ch4en48}. Jane Slaughter, “Interview with Henry
Louis Gates Jr., Harvard Professor,” *Progressive* 62, no. 1, Jan.
1998.</span>

<span>[49](#rch4en49){#ch4en49}. Yochai Benkler, *The Penguin and the
Leviathan: How Cooperation Triumphs over Self-Interest* (New York: Crown
Business, 2011), 212–23.</span>

<span>[50](#rch4en50){#ch4en50}. Naughton, *What You Really Need to Know
About the Internet*, 89. See also Joseph Michael Reagle Jr., *Good Faith
Collaboration: The Culture of Wikipedia* (Cambridge, MA: MIT Press,
2010).</span>

<span>[51](#rch4en51){#ch4en51}. Lawrence Lessig, *Republic, Lost* (New
York: Twelve Books, 2011), 34.</span>

<span>[52](#rch4en52){#ch4en52}. See, for example, Charles M. Schweik
and Robert C. English, *Internet Success: A Study of Open-Source
Software Commons* (Cambridge, MA: MIT Press, 2012).</span>

<span>[53](#rch4en53){#ch4en53}. MacKinnon, *Consent of the Networked*,
20.</span>

<span>[54](#rch4en54){#ch4en54}. James Losey e-mail to author, May 31,
2012.</span>

<span>[55](#rch4en55){#ch4en55}. Vaidhyanathan, *Googlization of
Everything*, 63. Tim Wu notes that *Wikipedia* entries consistently rank
at the very top of Google searches, ahead of the official sites for the
search term. See Wu, *Master Switch*, 287.</span>

<span>[56](#rch4en56){#ch4en56}. Somini Sengupta and Claire Cain Miller,
“Zuckerberg’s ‘Social Mission’ View vs. Financial Expectations of Wall
St.,” *New York Times*, Feb. 3, 2012, B1.</span>

<span>[57](#rch4en57){#ch4en57}. Wu, *Master Switch*, 244.</span>

<span>[58](#rch4en58){#ch4en58}. Much of the political fighting over the
Telecommunications Act was between the long-distance companies, like MCI
and Sprint, and the regional Baby Bells over the terms of
deregulation.</span>

<span>[59](#rch4en59){#ch4en59}. Wu, *Master Switch*, 245–46.</span>

<span>[](){#page_253}[60](#rch4en60){#ch4en60}. In 2012 Mississippi
passed a new law that, according to Brandon Presley, Northern District
Commissioner for the State of Mississippi, “will allow” AT&T “to raise
rates without any oversight at all.” Presley noted that AT&T was the
largest lobbying force in the state and got the bill they wrote. “We
have a coin-operated government,” Presley stated, adding, “That’s
wrong.” See Phillip Dampier, “Mississippi Public Service Commissioner on
Big Telecom \$: ‘We Have a Coin-Operated Government,” Stop the Cap!,
July 10, 2012,
[stopthecap.com/2012/07/10/mississippi-public-service-commissioner-on-big-telecom-we-have-a-coin-operated-government](http://stopthecap.com/2012/07/10/mississippi-public-service-commissioner-on-big-telecom-we-have-a-coin-operated-government).</span>

<span>[61](#rch4en61){#ch4en61}. Wu, *Master Switch*, 247.</span>

<span>[62](#rch4en62){#ch4en62}. Thanks to Derek Turner of Free Press
for his assistance with this section.</span>

<span>[63](#rch4en63){#ch4en63}. The FCC had only four members at the
time, as Democrat Jonathan Adelstein was waiting to be confirmed for the
open seat.</span>

<span>[64](#rch4en64){#ch4en64}. Meinrath, Losey, and Pickard, “Digital
Feudalism,” 434.</span>

<span>[65](#rch4en65){#ch4en65}. The information in this paragraph comes
from *Connecting America: The National Broadcasting Plan* (Washington,
DC: Federal Communications Commission, 2010), 37–38.</span>

<span>[66](#rch4en66){#ch4en66}. Ben Scott e-mail to author, June 14,
2012.</span>

<span>[67](#rch4en67){#ch4en67}. Sascha Meinrath e-mail to author, June
14, 2012.</span>

<span>[68](#rch4en68){#ch4en68}. Susan Crawford, “‘Survey: Mobile App
Privacy Fears Continue to Escalate,’”
[scrawford.net/blog](http://scrawford.net/blog), July 17, 2012,
[scrawford.net/blog/survey-mobile-app-privacy-fears-continue-to-escalate/1627](http://scrawford.net/blog/survey-mobile-app-privacy-fears-continue-to-escalate/1627).</span>

<span>[69](#rch4en69){#ch4en69}. Michael Moyer, “Verizon and AT&T
Accused of Being Threats to Democracy,” *Scientific American*, Mar. 13,
2012.</span>

<span>[70](#rch4en70){#ch4en70}. Brian X. Chen, “A Squeeze on
Smartphones,” *New York Times*, Mar. 2, 2012, B1, B4.</span>

<span>[71](#rch4en71){#ch4en71}. Brian X. Chen, “A Data Plan That
Devices Can Share,” *New York Times*, June 13, 2012, B1.</span>

<span>[72](#rch4en72){#ch4en72}. Matt Stoller, “Corruption Is
Responsible for 80% of Your Cell Phone Bill,” *Republic Report*, Apr.
11, 2012. Stoller gets his data from the Organization for Economic
Cooperation and Development (OECD), the FCC, and the former Cellular
Telecommunications Industry Association, now known only by its acronym,
CTIA.</span>

<span>[73](#rch4en73){#ch4en73}. Susan Crawford, “What’s Good for
Verizon and AT&T Is Terrible for American Consumers,” Wired Business,
July 26, 2012,
[wired.com/business/2012/07/whats-good-for-verizon-and-att-is-terrible-for-american-consumers](http://wired.com/business/2012/07/whats-good-for-verizon-and-att-is-terrible-for-american-consumers).
Harold Feld, the telecommunication policy expert for Public Knowledge,
wrote that “when every single incentive to profit maximization relies on
providing less service for more money and discouraging people from using
your service, something is seriously messed up.” Harold Feld, “The
Wireless Market Is Seriously Messed Up When Every Incentive Is
Anti-Consumer,” *Wetmachine*, July 24, 2012,
[tales-of-the-sausage-factory.wetmachine.com/the-wireless-market-is-seriously-messed-up-when-every-incentive-is-anti-consumer](http://tales-of-the-sausage-factory.wetmachine.com/the-wireless-market-is-seriously-messed-up-when-every-incentive-is-anti-consumer).</span>

<span>[74](#rch4en74){#ch4en74}. “Fortune 500,” CNN Money,
<http://money.cnn.com/magazines/fortune/fortune500/2011/full_list>.</span>

<span>[75](#rch4en75){#ch4en75}. Harold Feld, “My Insanely Long Field
Guide to the Verizon/SpectrumCo/Cox Deal,” *Wetmachine*, Mar. 22, 2012,
<http://tales-of-the-sausage-factory.wetmachine.com>.</span>

<span>[76](#rch4en76){#ch4en76}. Stacey Higginbotham, “Verizon to Buy
Cox Spectrum to Remake Its Broadband Model,” *Gigaom*,
<http://GigaOM.com>, Dec. 16, 2011.</span>

<span>[77](#rch4en77){#ch4en77}. Christopher Mitchell, *Broadband at the
Speed of Light: How Three [](){#page_254}Communities Built
Next-Generation Networks* (Washington, DC: Benton Foundation, Apr.
2012), 61.</span>

<span>[78](#rch4en78){#ch4en78}. Susan Crawford, “Water, Internet Access
and Swagger: These Guys Are Good,” [Wired.com](http://Wired.com), Mar.
9, 2012.</span>

<span>[79](#rch4en79){#ch4en79}. Feld, “My Insanely Long Field
Guide.”</span>

<span>[80](#rch4en80){#ch4en80}. Hayley Tsukayama, “Justice Allows
Verizon Deals with Cable Companies, with Conditions,” *Washington Post*
online, August 16, 2012,
[washingtonpost.com/blogs/post-tech/post/justice-approves-verizon-deal-with-cable-companies/2012/08/16/783aab14-e7a9-11e1-8487-64e4b2a79ba8\_blog.html](http://washingtonpost.com/blogs/post-tech/post/justice-approves-verizon-deal-with-cable-companies/2012/08/16/783aab14-e7a9-11e1-8487-64e4b2a79ba8_blog.html).</span>

<span>[81](#rch4en81){#ch4en81}. David Lazarus, “Why Is Verizon in Bed
with Time Warner and Comcast?” *Los Angeles Times* online, July 26,
2012,
[latimes.com/business/la-fi-lazarus-20120727,0,2605145.column](http://latimes.com/business/la-fi-lazarus-20120727,0,2605145.column).</span>

<span>[82](#rch4en82){#ch4en82}. Matthew Lasar, “Do We Need a New
National Broadband Plan?” *Ars Technica*, July 27, 2012,
[arstechnica.com/tech-policy/2012/07/do-we-need-a-new-national-broadband-plan](http://arstechnica.com/tech-policy/2012/07/do-we-need-a-new-national-broadband-plan).</span>

<span>[83](#rch4en83){#ch4en83}. Feld, “Wireless Market Is Seriously
Messed Up.”</span>

<span>[84](#rch4en84){#ch4en84}. Meinrath, Losey, and Pickard, “Digital
Feudalism,” 425.</span>

<span>[85](#rch4en85){#ch4en85}. For OECD data, see United Nations
Organisation for Economic Co-operation and Development, Directorate for
Science, Technology and Industry, OECD Broadband Portal,
<http://oecd.org>. See also: James Losey and Chiehyu Li, *Price of the
Pipe: Comparing the Price of Broadband Service Around the Globe*
(Washington, DC: New America Foundation, 2010).</span>

<span>[86](#rch4en86){#ch4en86}. Cited in Benjamin Lennett, Darah J.
Morris, and Greta Byrum, *Universities as Hubs for Next-Generation
Networks* (Washington, DC: New America Foundation, Apr. 2012), 2. The
authors of the report note that the U.S. rate lags behind peers even in
densely populated areas, refuting the claim that the vast space of the
United States accounts for its poor performance.</span>

<span>[87](#rch4en87){#ch4en87}. New America Foundation, *The Cost of
Connectivity* (Washington, DC: New America Foundation 2010), 1.</span>

<span>[88](#rch4en88){#ch4en88}. Lasar, “Do We Need a New National
Broadband Plan?”</span>

<span>[89](#rch4en89){#ch4en89}. Susan P. Crawford, “Team USA Deserves
No Gold Medal for Internet Access,” *Bloomberg View*, Aug. 5, 2012,
[bloomberg.com/news/2012-08-05/team-usa-deserves-no-gold-medals-for-internet-access.html](http://bloomberg.com/news/2012-08-05/team-usa-deserves-no-gold-medals-for-internet-access.html).</span>

<span>[90](#rch4en90){#ch4en90}. Sascha Meinrath e-mail to author, Aug.
13, 2012.</span>

<span>[91](#rch4en91){#ch4en91}. Pickard and Meinrath demonstrate that
there is a great deal of unused spectrum under government control that
could be put into circulation without harming the government’s needs.
See Victor W. Pickard and Sascha D. Meinrath, “Revitalizing the Public
Airwaves: Opportunistic Unlicensed Reuse of Government Spectrum,”
*International Journal of Communication* 3 (2009): 1052–84.</span>

<span>[92](#rch4en92){#ch4en92}. Nick Valery, “White-Space Puts Wi-Fi on
Steroids,” *The Economist*, Nov. 17, 2011, 48.</span>

<span>[93](#rch4en93){#ch4en93}. Richard Bennett, *Powering the Mobile
Revolution: Principles of Spectrum Allocation* (Washington, DC:
Information Technology and Innovation Foundation, 2012), 4, 5.</span>

<span>[94](#rch4en94){#ch4en94}. Meinrath e-mail to author, Aug. 13,
2012.</span>

<span>[95](#rch4en95){#ch4en95}. Feld, “My Insanely Long Field
Guide.”</span>

<span>[96](#rch4en96){#ch4en96}. Matt Wood e-mail to author, Aug. 22,
2012.</span>

<span>[97](#rch4en97){#ch4en97}. Karl Bode, “AT&T Wants FCC to Free More
Spectrum—for Them to Squat On,” *Broadband DSL Reports*, Jan. 14, 2011,
<http://dslreports.com>.</span>

<span>[](){#page_255}[98](#rch4en98){#ch4en98}. For a superior
examination of the spectrum issue, see Meinrath, Losey, and Pickard,
“Digital Feudalism,” 435, 437, 465, 466.</span>

<span>[99](#rch4en99){#ch4en99}. Peter Barnes, *Capitalism 3.0: A Guide
to Reclaiming the Commons* (San Francisco: Berrett-Koehler, 2006),
127.</span>

<span>[100](#rch4en100){#ch4en100}. See Brian Chen, “Carriers Warn of
Crisis in Mobile Spectrum,” *New York Times*, Apr. 17, 2012. Chen quotes
cell phone inventor Martin Cooper, who dismisses the claim as
unfounded.</span>

<span>[101](#rch4en101){#ch4en101}. MacKinnon, *Consent of the
Networked*, 120.</span>

<span>[102](#rch4en102){#ch4en102}. Bruce Upbin, “Complacent Telcos
Deliver Americans Third Rate Service at High Prices,” *Forbes* online,
July 21, 2012,
[forbes.com/sites/bruceupbin/2012/07/21/americans-suffer-from-third-rate-broadband-at-high-prices](http://forbes.com/sites/bruceupbin/2012/07/21/americans-suffer-from-third-rate-broadband-at-high-prices).</span>

<span>[103](#rch4en103){#ch4en103}. Council of Economic Advisers, *The
Economic Benefits of New Spectrum for Wireless Broadband* (Washington,
DC: Executive Office of the President, Feb. 2012).</span>

<span>[104](#rch4en104){#ch4en104}. Brian X. Chen, “Sharing the Air,”
*New York Times*, June 7, 2012, B1; and Brian X. Chen, “On Sharing the
Spectrum,” *New York Times*, June 4, 2012, B5.</span>

<span>[105](#rch4en105){#ch4en105}. James Losey e-mail to author, Aug.
13, 2012.</span>

<span>[106](#rch4en106){#ch4en106}. Meinrath e-mail to author, Aug. 13,
2012.</span>

<span>[107](#rch4en107){#ch4en107}. Josh Smith, “FCC Chairman Lobbies
Pentagon for More Spectrum,” *National Journal* online, Aug. 3, 2012,
[techdailydose.nationaljournal.com/2012/08/fcc-chairman-lobbies-pentagon.php](http://techdailydose.nationaljournal.com/2012/08/fcc-chairman-lobbies-pentagon.php).</span>

<span>[108](#rch4en108){#ch4en108}. E-mail from S. Derek Turner,
research director for Free Press, to the author, May 2, 2012.</span>

<span>[109](#rch4en109){#ch4en109}. Lynn Sweet, “Obama on Why He Is Not
for Single Payer Health Insurance: New Mexico Town Hall Transcript,”
*Chicago Sun Times*, May 14, 2009,
<http://blogs.suntimes.com/sweet/2009/05/obama_on_why_he_is_not_for_sin.html>.</span>

<span>[110](#rch4en110){#ch4en110}. Al Gore, “Networking the Future: We
Need a National ‘Superhighway’ for Computer Information,” *Washington
Post*, July 15, 1990, B3.</span>

<span>[111](#rch4en111){#ch4en111}. See Streeter, *Net Effect*,
106–15.</span>

<span>[112](#rch4en112){#ch4en112}. Gerry Smith, “Without Internet,
Urban Poor Fear Being Left Behind in Digital Age,” *Huffington Post*,
Mar. 1, 2012.</span>

<span>[113](#rch4en113){#ch4en113}. John Dunbar, “Poverty Stretches the
Digital Divide,” Investigative Reporting Workshop, American University
School of Communication, Mar. 23, 2012.</span>

<span>[114](#rch4en114){#ch4en114}. In June 2012, the Obama
administration announced plans to dramatically expand broadband access.
See Carl Franzen, “White House Debuts Ambitious Plan to Remake the Web
Using Broadband,” TPM Idea Lab, June 13, 2012, idealab.talk
[ingpointsmemo.com/2012/06/white-house-debuts-ambitious-plan-to-expand-broadband-again.php](http://ingpointsmemo.com/2012/06/white-house-debuts-ambitious-plan-to-expand-broadband-again.php).
After studying the measure, one policy expert told me it included
wonderful buzzwords but few actual requirements on (or competitive
challenges to) the cartel. “Like almost everything the Obama
administration has announced in recent years, \[it\] seems to be a
general whitewash for ‘leave it to the private sector and everything
will magically work out’ policies.”</span>

<span>[115](#rch4en115){#ch4en115}. Lennett, Morris, and Byrum,
*Universities as Hubs*, 3.</span>

<span>[116](#rch4en116){#ch4en116}. Ibid.</span>

<span>[117](#rch4en117){#ch4en117}. The material in this paragraph comes
from Mitchell, *Broadband at the Speed of Light.*</span>

<span>[118](#rch4en118){#ch4en118}. Denise Roth Barber, “Dialing Up the
Dollars: Telecommunication Interests Donated Heavily to NC Lawmakers,”
National Institute on Money in State Politics, Mar. 20, 2012,
[followthemoney.org/press/ReportView.phtml?r=484](http://followthemoney.org/press/ReportView.phtml?r=484).</span>

<span>[119](#rch4en119){#ch4en119}. “Municipal Broadband: Triumph of the
Little Guys,” *The Economist* online, [](){#page_256}Aug. 10, 2012,
[economist.com/blogs/democracyinamerica/2012/08/municipal-broadband](http://economist.com/blogs/democracyinamerica/2012/08/municipal-broadband).</span>

<span>[120](#rch4en120){#ch4en120}. Masha Zager, “Santa Monica City Net:
How to Grow a Network,” *Broadband Communities*, May–June 2011,
44–47.</span>

<span>[121](#rch4en121){#ch4en121}. Higginbotham, “Verizon to Buy Cox
Spectrum.”</span>

<span>[122](#rch4en122){#ch4en122}. Wu, *Master Switch*, 285.</span>

<span>[123](#rch4en123){#ch4en123}. MacKinnon, *Consent of the
Networked*, 121.</span>

<span>[124](#rch4en124){#ch4en124}. Erick Schonfeld, “Vint Cerf Wonders
If We Need to Nationalize the Internet,” *TechCrunch*, June 25, 2008,
[techcrunch.com/2008/06/25/vint-cerf-wonders-if-we-need-to-nationalize-the-internet](http://techcrunch.com/2008/06/25/vint-cerf-wonders-if-we-need-to-nationalize-the-internet).</span>

<span>[125](#rch4en125){#ch4en125}. Citation and link to video of Obama
statement in: Timothy Karr, “Obama FCC Caves on Net Neutrality—Tuesday
Betrayal Assured,” *Huffington Post*, Dec. 20, 2010,
[huffingtonpost.com/timothy-carr/obama-fcc-caves-on-net-ne\_b\_799435.html](http://huffingtonpost.com/timothy-carr/obama-fcc-caves-on-net-ne_b_799435.html).</span>

<span>[126](#rch4en126){#ch4en126}. Jeff Jarvis, *Public Parts: How
Sharing in the Digital Age Improves the Way We Work and Live* (New York:
Simon & Schuster, 2011), 208.</span>

<span>[127](#rch4en127){#ch4en127}. MacKinnon, *Consent of the
Networked*, 122.</span>

<span>[128](#rch4en128){#ch4en128}. Jarvis, *Public Parts*, 208.</span>

<span>[129](#rch4en129){#ch4en129}. Charles Arthur, “Walled Gardens Look
Rosy for Facebook, Apple—and Would-Be Censors,” *The Guardian*, Apr. 17,
2012.</span>

<span>[130](#rch4en130){#ch4en130}. Ibid.</span>

<span>[131](#rch4en131){#ch4en131}. Eli M. Noam, *Media Ownership and
Concentration in America* (New York: Oxford University Press, 2009),
365–69.</span>

<span>[132](#rch4en132){#ch4en132}. Barry C. Lynn, “Killing the
Competition: How the New Monopolies Are Destroying the Competition,”
*Harper’s*, Feb. 2012, 33.</span>

<span>[133](#rch4en133){#ch4en133}. Mergent Online, Moody’s Investors
Service, 2012, by subscription at <http://mergentonline.com> (accessed
Apr. 24, 2012). I chronicle in detail the extent and nature of these
media empires in the late 1990s in Robert W. McChesney, *Rich Media,
Poor Democracy: Communication Politics in Dubious Times* (New York: The
New Press, 2000), chap. 1.</span>

<span>[134](#rch4en134){#ch4en134}. I say “qualified” because although
the Fortune list for 2000 technically did not include foreign-based
media conglomerates with huge U.S.-based interests, like Sony, Vivendi,
and Bertelsmann, I’ve included them for the sake of accuracy. This
approach is meant to provide a rough idea of the increase in the number
of media powerhouses, and not to be regarded as the final word on the
matter. For a detailed statistical assessment, see Noam, *Media
Ownership and Concentration.*</span>

<span>[135](#rch4en135){#ch4en135}. The classic work was Ben H.
Badgikian, *The Media Monopoly* (Boston: Beacon Press, 1983), which has
enjoyed many subsequent revised editions.</span>

<span>[136](#rch4en136){#ch4en136}. They also became conglomerates
because media economics, as discussed in chapter 3, are rather unlike
traditional markets for goods and services. Conglomeration is an
especially effective way to reduce risk. An excellent discussion of this
is in Wu, *Master Switch*, ch. 17.</span>

<span>[137](#rch4en137){#ch4en137}. Jaron Lanier, *You Are Not a Gadget:
A Manifesto* (New York: Knopf, 2010), 87.</span>

<span>[138](#rch4en138){#ch4en138}. Steven Levy, “How the Propeller
Heads Stole the Electronic Future,” *New York Times Magazine*, Sept. 24,
1995, 58.</span>

<span>[139](#rch4en139){#ch4en139}. Lanier, *You Are Not a Gadget*,
87.</span>

<span>[140](#rch4en140){#ch4en140}. Adam Smith, *The Wealth of Nations*
(1776; New York: Modern Library, 1937), 173.</span>

<span>[141](#rch4en141){#ch4en141}. This episode is chronicled in John
Motavalli, *Bamboozled at the Revolution: [](){#page_257}How Big Media
Lost Billions in the Battle for the Internet* (New York: Viking, 2002).
See also *Rich Media, Poor Democracy*, chap. 3.</span>

<span>[142](#rch4en142){#ch4en142}. One striking development is that
media conglomerates are moving into education as a potential “cash cow”
for textbooks and, especially, digital material. Rupert Murdoch’s News
Corporation hired former New York City schools chancellor Joel Klein to
run Amplify, its for-profit education subsidiary. The entry of the
conglomerates into education suggests an even greater opening toward
commercialism and commercial values. It is an existing \$7 billion
market, a public trough for the giants to battle over. See Brooks Barnes
and Amy Chozick, “The Classroom as a Cash Cow,” *New York Times*, Aug.
20, 2012, B1, B8.</span>

<span>[143](#rch4en143){#ch4en143}. “Fortune 500,” CNN Money,
[money.cnn.com/magazines/fortune/fortune500/2011/full\_list](http://money.cnn.com/magazines/fortune/fortune500/2011/full_list).</span>

<span>[144](#rch4en144){#ch4en144}. MacKinnon, *Consent of the
Networked*, 101.</span>

<span>[145](#rch4en145){#ch4en145}. See Tarleton Gillespie, *Wired Shut:
Copyright and the Shape of Digital Culture* (Cambridge, MA: MIT Press,
2007).</span>

<span>[146](#rch4en146){#ch4en146}. Ian Katz, “Web Freedom Faces
Greatest Threat Ever, Warns Google’s Sergey Brin,” *The Guardian*, Apr.
15, 2012.</span>

<span>[147](#rch4en147){#ch4en147}. David Kravets, “Analysis: Internet
Blacklist Bill Is Roadmap to ‘the End’ of the Internet,” *Wired*, Nov.
17, 2011.</span>

<span>[148](#rch4en148){#ch4en148}. See, for example, Brooke,
*Revolution Will Be Digitised*, 47; and Dominic Rushe, “The Online
Copyright War: The Day the Internet Hit Back at Big Media,” *The
Guardian*, Apr. 18, 2012.</span>

<span>[149](#rch4en149){#ch4en149}. Harold Feld, “Op-ed: MPAA/RIAA Lose
Big as US Backs Copyright ‘Limitations,’” Ars Technica, July 8, 2012,
[arstechnica.com/tech-policy/2012/07/op-ed-eus-rejection-of-acta-subtly-changed-trade-law-landscape](http://arstechnica.com/tech-policy/2012/07/op-ed-eus-rejection-of-acta-subtly-changed-trade-law-landscape).</span>

<span>[150](#rch4en150){#ch4en150}. Losey e-mail to author, Aug. 13,
2012.</span>

<span>[151](#rch4en151){#ch4en151}. Meinrath e-mail to author, Aug. 13,
2012.</span>

<span>[152](#rch4en152){#ch4en152}. MacKinnon, *Consent of the
Networked*, 104–11.</span>

<span>[153](#rch4en153){#ch4en153}. Antoine Champagne, “Watching Over
You: The Perils of Deep Packet Inspection,” *CounterPunch*, Mar. 8,
2012.</span>

<span>[154](#rch4en154){#ch4en154}. Brendan Greeley, “Hollywood Tries to
Wash the Web with SOPA,” *Bloomberg Businessweek*, Dec. 19–25, 2011,
35–36. The conclusions were reached in a four-hundred-page report
published by the Social Science Research Council in January 2011.</span>

<span>[155](#rch4en155){#ch4en155}. David D. Friedman, *Future
Imperfect: Technology and Freedom in an Uncertain World* (New York:
Cambridge University Press, 2008), 16.</span>

<span>[156](#rch4en156){#ch4en156}. Patricia Aufderheide and Peter
Jaszi, *Reclaiming Fair Use: How to Put Balance Back in Copyright*
(Chicago: University of Chicago Press, 2011); Benkler, *Penguin and the
Leviathan*, 222–29; and Lawrence Lessig, *Remix: Making Art and Commerce
Thrive in the Hybrid Economy* (New York: Penguin Press, 2008).</span>

<span>[157](#rch4en157){#ch4en157}. Rushe, “Online Copyright
War.”</span>

<span>[158](#rch4en158){#ch4en158}. Losey e-mail to author, Aug. 13,
2012. For a series of concerns about the prospective plan, see Douglas
Rushkoff, “Will Your Internet Provider Be Spying on You?” CNN Opinion,
July 6, 2012,
[cnn.com/2012/07/06/opinion/rushkoff-online-monitoring](http://cnn.com/2012/07/06/opinion/rushkoff-online-monitoring).</span>

<span>[159](#rch4en159){#ch4en159}. Amy Chozick, “Under Copyright
Pressure, Google to Alter Search Results,” *New York Times*, Aug. 11,
2012, B2.</span>

<span>[160](#rch4en160){#ch4en160}. Carl Franzen, “Google’s Copyright
Filtering Causes Concern,” Talking Points Memo, Aug. 10, 2012.</span>

<span>[161](#rch4en161){#ch4en161}. Wood e-mail to author.</span>

<span>[](){#page_258}[162](#rch4en162){#ch4en162}. Greeley, “Hollywood
Tries to Wash the Web,” 36.</span>

<span>[163](#rch4en163){#ch4en163}. Adler, *Updating Rules of the
Digital Road*, 2.</span>

<span>[164](#rch4en164){#ch4en164}. Steve Wasserman, “The Amazon
Effect,” *The Nation*, May 29, 2012. See also Julie Bosman, “Survey
Details How E-Books Continue Strong Growth Trend,” *New York Times*,
July 19, 2012, B4.</span>

<span>[165](#rch4en165){#ch4en165}. Larry Downes perceptively notes that
in this environment it is difficult for people to have any respect for
copyright: “\[Copyright\] no longer holds any moral authority with most
consumers. There’s no longer an ethical imperative to obey it or even
understand it. Self-enforcement is fading, and the rules are so severe
and so frequently violated that effective legal enforcement has become
nearly impossible. . . .</span>

<span>“Copyright is a law in name only—as obsolete and irrelevant as
rules still on the books in some jurisdictions that regulate who can or
must wear what kind of clothing.” See Larry Downes, “How Copyright
Extension Undermined Copyright: The Copyright of Parking (Part 1),”
*Techdirt*, May 21, 2012,
[techdirt.com/articles/20120521/03153118987/how-copyright-extension-undermined-copyright-copyright-parking-part-i.shtml](http://techdirt.com/articles/20120521/03153118987/how-copyright-extension-undermined-copyright-copyright-parking-part-i.shtml).</span>

<span>[166](#rch4en166){#ch4en166}. Lanier, *You Are Not a Gadget*, 89,
91.</span>

<span>[167](#rch4en167){#ch4en167}. Ben Sisario, “New Layer of Content
Amid Chaos on YouTube,” *New York Times*, Mar. 12, 2012, B1.</span>

<span>[168](#rch4en168){#ch4en168}. Evan Shapiro, “The 8 Most Important
Things to Happen to TV in the Past 5 Years,” *Huffington Post*, Mar. 8,
2012,
[huffingtonpost.com/even-shapiro/tvs-top-8\_b\_1328846.html](http://huffingtonpost.com/even-shapiro/tvs-top-8_b_1328846.html).</span>

<span>[169](#rch4en169){#ch4en169}. “Charlie Rose Talks to Hulu CEO
Jason Kilar,” *Bloomberg Businessweek*, Mar. 5–11, 2012, 48.</span>

<span>[170](#rch4en170){#ch4en170}. RBC Capital Markets, “Brightcove,
Inc.: Introducing the Video Cloud,” Report for Subscribers, Mar. 28,
2012, 1.</span>

<span>[171](#rch4en171){#ch4en171}. Turow, *Daily You*, 161.</span>

<span>[172](#rch4en172){#ch4en172}. Bill Carter, “Where Have All the
Viewers Gone?” *New York Times*, Apr. 23, 2012, B1, B3.</span>

<span>[173](#rch4en173){#ch4en173}. Kit Eaton, “The Future of TV Is Two
Screens, One Held Firmly in Your Hands,” *Fast Company* online, July 17,
2012,
[fastcompany.com/1842995/future-tv-two-screens-one-held-firmly-your-hands](http://fastcompany.com/1842995/future-tv-two-screens-one-held-firmly-your-hands).</span>

<span>[174](#rch4en174){#ch4en174}. Brian Stelter, “New Internet TV
Network to Feature Larry King,” *New York Times*, Mar. 12, 2012,
B5.</span>

<span>[175](#rch4en175){#ch4en175}. Nick Bilton, “In a Skirmish to
Control the Screens,” *New York Times*, June 4, 2012, B5.</span>

<span>[176](#rch4en176){#ch4en176}. Nick Bilton, “TV Makers Ignore Apps
at Their Own Peril,” *New York Times*, Mar. 12, 2012, B4; and Mike Hale,
“Genres Stretch, for Better and Worse, as YouTube Takes On TV,” *New
York Times*, Apr. 25, 2012, A1, A3. For a thoughtful treatment of the
dilemma the Internet has created for media firms and content production,
see Robert Levine, *Free Ride: How Digital Parasites Are Destroying the
Culture Business, and How the Culture Business Can Fight Back* (New
York: Anchor, 2011).</span>
