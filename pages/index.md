<!--
.. title: Reading list
.. slug: index
.. date: 2017-04-23 20:10:34 UTC+02:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

This is the reading list for Different Net Critique. Texts here are converted to HTML so it is easier to collaboratively annotate them.

 - [The Control Revolution: Technological and Economic Origins of the Information Society by James Beniger](/pages/the-control-revolution/) 
 - [Digital Disconnect: How Capitalism Is Turning the Internet Against Democracy by Robert Waterman McChesney](/pages/digital-disconnect/) 
 - [Control and Freedom: Power and Paranoia in the Age of Fiber Optics by Wendy Hui Kyong Chun](/pages/control-and-freedom/) 
 - [Prehistory of the cloud by Tung-Hui Hu](/pages/time-sharing-and-virtualization)
 - [Amazon Economy by Barney Jopson](/pages/amazon-economy)
 - [Confronting Amazon by Ralf Rukus](/pages/confronting-amazon)

## Bookmarks

[Zotero](https://www.zotero.org/groups/1207007/amazoncourse/items)

## Short description of the course

> Global computer network infrastructure is ubiquitous and its development is not likely to stop any time soon. For almost any particular user of that infrastructure there is an overwhelming abundance of available information. However, there is something that the bird's-eye view of the infrastructure does not convey and design of that infrastructure follows very specific geo-political and geo-economic arrangements and divides, in the legal regulation of non-democratic interests of corporations and / or goverments. Global computer network infrastructure (with all those patterns) thus shows asymmetries of inequality,

> Our case study in that work of net critique wants to be Amazon.com. Amazon.com has turned the book from a bookshelf item into a reimbursing streaming service, a retail store into a search engine for products and their prices. It is transforming global informational, material and energy flows like almost no other company in the world. It is becoming itself to become the universal utility company of retail and online services world. Amazon.com's digital computer network infrastructure: from ebooks to the cloud.

