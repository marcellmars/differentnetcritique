<!--
.. title: Different Net Critique - reading list
.. slug: index
.. date: 2017-04-23 20:10:34 UTC+02:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

This is the reading list for Different Net Critique. Texts here are converted to HTML so it is easier to collaboratively annotate them.

 - [The Control Revolution: Technological and Economic Origins of the Information Society by James Beniger](/pages/the-control-revolution/) 
 - [Digital Disconnect: How Capitalism Is Turning the Internet Against Democracy by Robert Waterman McChesney](/pages/digital-disconnect/) 
 - [Control and Freedom: Power and Paranoia in the Age of Fiber Optics by Wendy Hui Kyong Chun](/pages/control-and-freedom/) 
 - [Prehistory of the cloud by Tung-Hui Hu](/pages/time-sharing-and-virtualization)
 - [Amazon Economy by Barney Jopson](/pages/amazon-economy)
 - [Confronting Amazon by Ralf Rukus](/pages/confronting-amazon)
