<!--
.. title: Confronting Amazon by Ralf Rukus
.. slug: confronting-amazon
.. date: 2017-06-21 19:02:38 UTC+02:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

[Originally published for Jacobin](https://www.jacobinmag.com/2016/03/amazon-poland-poznan-strikes-workers/)

#Confronting Amazon

**Through creative actions and cross-border solidarity, Polish workers are undermining Amazon's anti-union playbook.**


Since its humble beginnings as an online bookseller, Amazon has become a household name — synonymous with endless product choices and same-day shipping, all sustained by a vast logistics network that spans continents.

[Amazon](https://www.jacobinmag.com/2015/08/amazon-google-facebook-privacy-bezos/) — which now has more than ninety fulfillment, redistribution, and sorting centers in the US — began opening warehouses in Europe in 1999. Five years later, it moved into Asia, establishing centers in Japan and China, and now India — bringing its total number of warehouses to more than two hundred.

What’s peculiar about Amazon is that the company’s enormous size isn’t matched by huge profits. So far it has dumped most of its surplus into building facilities, creating product lines, and developing new technologies.

But that’s where the oddity ends. While Amazon’s sales revenue and employment figures still lag far behind [Walmart’s](https://www.jacobinmag.com/2015/03/liza-featherstone-walmart-unions-workers/) — about \$90 billion and 150,000 workers, compared to Walmart’s \$490 billion and well over two million employees — the two giants share a core philosophy about work relations. The more you exploit workers, the retailers agree, the bigger your profit margins.

Amazon warehouse workers, however, are beginning to fight back. One of the key sites of struggle is Poland, where employee strikes and work slowdowns are upsetting ossified unions and Amazon management alike. But can workers in the Eastern European country go toe-to-toe with an international behemoth?

##Amazon in Poland

Germany and the United Kingdom continue to anchor [Amazon’s European expansion](https://www.internetretailer.com/2015/04/30/amazon-builds-its-european-marketplace). But Poland is playing an increasingly critical role in the company’s push to serve the German market, its second largest.

In fall 2014, Amazon built three new fulfillment centers — large warehouses where products are picked, packed, and shipped to consumers — in western Poland. One center is located in Sady, near the city limits of Poznań, and the other two are close to the city of Wrocław. In the lead-up to the warehouse openings, Amazon announced it would bring on six thousand permanent workers and up to nine thousand temporary employees during peak times.

When the Poznań fulfillment center opened in September 2014, shop-floor workers were hired directly by Amazon on three-month contracts and then became permanent workers a few months later. New hires had just a few days of training before they were turned loose on the warehouse floor, and starting in October, Amazon began hiring workers through a handful of temp agencies.

Employing a mix of permanent and temporary workers is central to Amazon’s [workplace model](http://highline.huffingtonpost.com/articles/en/life-and-death-amazon-temp/), which divides workers between those employed by Amazon itself on a permanent contract (marked by a blue badge) and workers that temporary work agencies bring on for a few weeks or months (marked by a green badge). Off-peak, temporary workers constitute at least half the workforce, and during the busiest stretch (November through January), they at least double the number of permanent workers.

The male-to-female ratio in Poland is fairly equal (although this varies by department), and most workers are either in their twenties or over forty-five, reflecting the characteristics of the broader labor market. For twenty-somethings, working in the Amazon warehouse is typically a first-time or seasonal job; for older workers it’s often a last-ditch attempt at employment. Most workers are Polish, but a few come from other countries, such as Ukraine. Foreign employees generally work as janitors, or fill subcontracted positions in the warehouse or in cafeteria.

In the Poznań warehouse, the workweek is divided into four ten-hour day and evening shifts, plus overtime. Shifts are Sunday to Wednesday, Monday to Thursday, and Wednesday to Saturday; workers switch from the day shift to the night shift every four weeks.

Like other logistics companies, Amazon organizes its departments according to “inbound” and “outbound” work. Inbound tasks include unloading trucks with forklifts, unpacking and scanning goods, and stowing products in the warehouse; outbound work involves picking goods from the shelves to match customer orders, and packing, loading, and shipping the products. Both departments rely on conveyor belts, scanners, and computers, but most of the work (grabbing and scanning items, running from one spot to another in the warehouse, checking information on handheld scanners) is “unskilled” and done by hand.

Amazon’s model of work organization isn’t entirely new. Even before the introduction of computer-controlled systems, catalog companies like Sears used an analogous system in its warehouses. But the Seattle company has capitalized on the standardization of transport systems (containerization), the mainstreaming of online services (ordering via central online platforms), and the Taylorization and restructuring of [warehouse work](https://www.jacobinmag.com/2015/11/amazon-fedex-ups-post-office-teamsters/) (digital surveillance, scanning codes, computer-controlled conveyor belts, stowing and picking robots) to capture an increasing share of the retail market.

And like most other big companies, Amazon’s organizational innovations depend on squeezing as much work out of employees as possible. At Amazon, every department in the fulfillment center sets an hourly quota. The company doesn’t reveal how quotas are determined.

While managers say they calculate the minimum pace for individuals based on the average output of 90 percent of the most productive workers, they don’t divulge how the collective target — the rate demanded for all workers — is set.

All of the workers in the warehouse must reach the target to get a wage bonus. The minimum performance level, meanwhile, is used to pressure individual workers. Those who fail to meet it have a “feedback talk” with managers and then face termination if they don’t improve.

Amazon also frequently raises the quota, ratcheting up the pressure to complete tasks at a faster and faster pace. As one explained, “When I work too slow I get a kind of [text message] to my scanner: ‘Work faster!’”

When the fulfillment center in Poznań first opened, the pay for warehouse workers was 13 Polish Złoty (PLN) per hour, or about 2,000 PLN ($480) a month after bonuses and taxes. This is higher than Poland’s minimum wage (which in 2015 was 1,750 PLN, or $420 per month before taxes), and Amazon team leaders make about 20 to 25 percent more than their co-workers. But while all temporary workers receive the same pay they don’t all receive bonuses. And though enough for a single person, 2,000 PLN is hardly sufficient to support a family in Poland.

##Organizing in Poznań

In Poznań, workers were initially trained by Amazon team leaders from abroad, and the Polish workers who were employed as team leaders were sent to learn at fulfillment centers in the UK and Germany. This interaction gave Polish workers firsthand information about wage levels and working conditions abroad. As one explained, “Amazon tells us: ‘You are the best workers in Europe!’ We ask: ‘Why, then, do we get the lowest wages in Europe?’”

After a few weeks on the job, some workers and team leaders in Poznań began discussing their dissatisfaction with the working conditions, as well as possible ways to organize and fight for improvements.

One suggestion was to organize inside the warehouse with the help of Solidarność (Solidarity), Poland’s largest union. But most workers — viewing the storied organization as largely passive and more interested in nationalist and conservative religious issues than workers’ struggles — opposed seeking its assistance. Younger members of Amazon’s workforce, some with higher education and work experience in Western Europe, considered Solidarność out of touch, a bunch of “old union men with mustaches.”

An Internet search turned up an alternative: the [Inicjatywa Pracownicza](http://www.ozzip.pl/) (Workers’ Initiative). Impressed by reports about the militant workers’ struggles the union was involved in — as well as the [IP’s logo](http://ozzip.pl/templates/ozzip/images/logo.png), which features a fierce-looking black cat — the Amazon workers arranged a mid-December meeting.

One worker later recalled that workers had decided beforehand that if the IP office was in a nice, big building they would know the unionists were “thieves” and would search for another option. But the IP office was in a tiny room in an old building.

In fact, a few IP activists were already working at Amazon and had been involved in organizing drives and support campaigns elsewhere. They’d also distributed leaflets about Amazon’s employment practices to workers in the fall (without using the union’s name or logo).

Soon after their initial meeting, the discontented workers and the handful of IP activists employed at Amazon met in the Poznań fulfillment center’s parking lot to form an IP union section. While they had never engaged in this kind of labor action, warehouse workers outside the IP took the lead; the IP activists registered the union section, wrote and printed leaflets, and dealt with management. The shared experience of working at Amazon provided the basis for collaboration.

Within a year, the IP section at Amazon’s Poznań warehouse grew from 20 members to more than 350. Most are permanent shop-floor workers (a minority are temp workers, and a few are team leaders). Amazon only knows the 15 names of the elected IP shop stewards, who are statutorily protected from dismissal; the rest are kept secret to prevent management retaliation.

In early 2015, the IP section wrote and distributed leaflets discussing problems at work and information about workers’ legal rights, while other employees (some of whom were not even union members) initiated petitions that at times received hundreds of signatures. The petitions dealt with workers’ main grievances — low wages, rising quotas, changes in shift schedules, and working on public holidays.

Amazon has responded to the organizing drive in a variety of ways. As elsewhere (US, UK, Germany) it has made no official concessions to the union, while doing everything from ignoring organizing attempts and avoiding open confrontation to trying to isolate union activity and engaging in anti-union maneuvering. It has also begun promoting its own company-wide system of employee representation at its Polish warehouses called “Forum Pracownicze” (Workers’ Forum).

##Networking in Poland and Beyond

While the IP has won hundreds of members in Poznań, it has little presence in the two warehouses near Wrocław. There, Solidarność is the legal representative of the the workforce and has about one hundred members.

IP is open to collaborate with Solidarnosc members to push for better worker benefits and stronger health and safety regulations. But despite the IP’s attempts to stay in contact with Solidarność’s Amazon workers, this has proven challenging.

Solidarność has issued public statements attacking IP for being too “confrontational” and “irresponsible” and presents itself as a conciliatory alternative, willing to negotiate and work with management.

Since the fulfillment centers in Poland mainly serve the German market, the IP has also tried to link up with workers in Amazon warehouses there. Verdi — a large German service union with two million members — has been organizing [short-term strikes](http://www.reuters.com/article/us-amazon-com-germany-idUSKBN0U40SY20151221) since 2013 in an attempt to pressure Amazon to adhere to Verdi’s collective bargaining agreement with Germany’s mail-order and retail industry to raise employee pay. Though Amazon has increased wages since the strikes began, it refuses to sign a retail sector agreement, insisting that its warehouse workers are “logistics workers.”

Verdi is known for its willingness to work with management to reach mutually beneficial arrangements (known as ‟social partnership” agreements), as well as for tightly controlling worker mobilizations and halting worker actions that threaten to get out of hand.

But Verdi has seen its position in retail weakened due to the decline of unionized companies and the rise of new corporations like Amazon, which have resisted organizing efforts. At the same time, members are increasingly dissatisfied with the way Verdi conducts workplace struggles, and have started looking to alternative sectoral unions.

When IP activists in Poland initially reached out to workers at Amazon’s German warehouses, they weren’t sure what to expect. Verdi partners with Solidarność through the inter-union organization Uniglobal, but Verdi workers at fulfillment centers in Bad Hersfeld, Brieselang, and Leipzig had expressed interest in establishing worker-to-worker links with members of the IP section before they made contact.

Bad Hersfeld workers in particular have established some autonomy from Verdi’s bureaucracy and have built ties with outside activist groups. For instance, members of the [Blockupy campaign](http://www.theguardian.com/world/gallery/2015/mar/18/blockupy-anti-ecb-protest-in-frankfurt-in-pictures) in nearby Frankfurt sent a small delegation to support striking Amazon workers.

After an initial meeting in March 2015, several cross-border meetings between activists and Amazon workers from Poznań, Bad Hersfeld, Brieselang, and Leipzig have taken place. The gatherings, attended by ten to thirty workers (and a number of their supporters), have made it possible for workers to correct misconceptions and find common ground: Polish now workers see that German warehouse workers share the same grievances, and German workers are no longer afraid the company will relocate all of its its German warehouses to “cheap labor” Poland.

##Work Action

The cross-border meetings have also facilitated labor actions.

In late June 2015, in response to a Verdi strike in Germany, Amazon management in Poznań tried to make Polish employees work overtime to pick up the slack. The tactic was nothing new. In the past, Amazon had shifted the handling of orders between fulfillment centers during strike days (as well as in the face of supply or delivery problems, or delays due to bad weather).

But this time, workers in Poznań knew the impetus for the forced overtime and publicized it widely on blackboards, banners, and flyers. In Poznań, tensions had been rising for months, and many employees had strengthened their relationships with each other through collecting petition signatures and working on the IP organizing drive.

Days before the increased overtime kicked in, workers and managers alike knew something was up: employees were discussing ideas on the shop floor, on the company bus, and even on Facebook about how to slow down the work process.

On the night forced overtime was set to begin, IP shop stewards showed up to work wearing Verdi strike t-shirts. Management promptly sent them to a training session, eager to get them off the shop floor.

But other workers, determined to go ahead with the action, exploited a weakness in the workflow process. In the pick department, workers normally place multiple items into bins, which are then put on the conveyor belt. That night, however, they dropped them in one at a time, quickly clogging the conveyor belt and causing one-item bins to begin tumbling from the belt.

After a big pile of bins and items formed, the belt had to be switched off — right in front of the general manager, who, expecting trouble that night, had made a special trip to the warehouse to supervise the workers. Also undeterred, workers elsewhere in the warehouse were carrying out similar actions.

The dozens of workers who carried out the wildcat action — some of whom didn’t belong to the IP and hadn’t engaged in this kind of struggle before — declined to make specific demands. But they described the slowdown as the result of frustration, as well as an act of solidarity with the striking workers in Germany. News of the action spread quickly in the warehouse, and many workers were enthusiastic, knowing that they’d shown they would no longer quietly accept every company order.

The German Amazon workers who had been participating in the cross-border meetings were also impressed with the action. Since the slowdown, there has been talk of similar actions in German fulfillment centers in solidarity with labor actions at warehouses in Poznań.

The action also got the media’s attention. Both Polish and international media noted that the slowdown was the first expression of worker militancy in Amazon’s Polish fulfillment centers. Solidarność, meanwhile, played the part of the staid union. Its section from the Wrocław warehouses issued a press release a few days later condemning the action.

Management responded by interrogating workers who had taken part in the action, suspending five workers and later firing two. On the whole, though, they’ve tried to isolate activists by intimidating unorganized workers instead of attacking them directly.

But this tactic could backfire. As one woman who participated in the slowdown said during her interrogation: “I will do it again if we are forced to do overtime again!” Two workers have also taken Amazon to labor court demanding reinstatement, backed by the IP.

The IP officially opened the collective bargaining process with Amazon the day after the slowdown and presented the workers’ demands, which included a wage increase to 16 PLN per hour and longer breaks. But the strike had soured Amazon on the IP. During an official employee assembly in Poznań, Amazon’s general manager said the company would “prefer to talk to the workers directly and not via the union.”

Despite several bargaining meetings in late summer and early fall 2015, the IP was unable to make much headway. Amazon management refused to consider any of the workers’ demands, let alone sign an official agreement with the union. Amazon then promptly declared the mediation that followed a failure, preventing the IP from organizing a legal two-hour warning strike during the mediation period. The warning strike would have given the union the opportunity to mobilize workers against Amazon’s intransigence.

With the mediation process officially closed, the IP would have to go through the laborious process of organizing a formal strike: it would need to hold an authorization vote in each Polish fulfillment center (including the warehouses near Wrocław), and the majority of workers would have to affirm their support.

At the moment, it is not clear whether IP’s Amazon section will follow that path or whether it can garner enough support on the shop floor. In the meantime, workers are focusing on everyday shop floor activism and engaging other employees through petitioning and flyering.

The IP’s steady presence, and its continued pressure on Amazon, have secured some modest gains for workers. A few weeks after the slowdown, Amazon boosted the hourly wage from 13 to 14 PLN an hour (both in Poznań and Wrocław), and in late fall the company increased worker bonuses. In other cases, the company has reversed or postponed decisions that workers found objectionable, such as its plan to change shift hours.

##Challenges in Poznań

After a year of organizing efforts at the Poznań fulfillment center, workers are facing five major challenges.

First, problems of communication and assemblies. IP’s Amazon workers have found it difficult to have a general assembly — a necessity since all workers are never off at the same time, and most IP members work the same shift. Furthermore, after working a ten-hour shift, enduring a commute of up to four hours, and juggling family responsibilities, workers are often too exhausted to attend meetings.

Second, the relationship between permanent workers and temporary workers. While the majority of the workers in the IP section are permanent Amazon employees, a significant number of workers are temps employed for brief periods (with some exceptions). Temporary workers have said they don’t want to engage in organizing because the job is only short term, and while they’re angry about their working conditions and their insecure job status, they’re also hoping for permanent employment from Amazon.

This attitude sometimes leads to conflicts with permanent workers, who have more job security and more “control” over quotas. During the pre-Christmas peak last year, permanent workers were promoted to easier positions without quotas while temporary workers filled the positions with daily quotas. The tables turned earlier this year, when pressure increased on permanent workers. They began to receive work tasks with quotas and, in some cases, temps were tasked with overseeing permanent workers.

IP’s Amazon section, seizing on the issue of precariousness, has organized rallies outside the offices of the temporary work agency Adecco in Poznań and Warszawa. It has also demanded that Amazon limit the proportion of temporary workers.

A divided labor force gives Amazon the best of both worlds: it can hire and fire its [temporary workers](http://www.wired.com/2015/10/amazon-is-hiring-a-bonkers-number-of-temps-this-christmas/) according to the ups and downs of online sales, while counting on its permanent section to maintain the knowledge and skills needed to run the warehouses.

A third challenge is exhaustion among permanent workers. Over the past year, some of the permanent workers most active in IP’s Amazon section have quit their jobs out of frustration or because they found better work elsewhere.

Many of those who are still especially involved are completely drained. During the 2015 pre-Christmas peak, many workers from IP’s Amazon section were out sick, making organizing difficult. A December rally in Poznań, for instance, was sparsely attended.

Fourth, there’s the balance between effective strategies and legal boundaries. The IP can’t claim official responsibility for wildcat actions, and the legal work it carries out — such as the stalled negotiations with Amazon — have had little effect. Wildcat actions, however, exert direct and immediate pressure on Amazon even if they include more risks for the workers involved.

The larger question, then, is this: how do workers negotiate the tension between the limitations and predictability of official union actions and the efficiency of disruptive wildcat actions in the context of a long-term struggle against a company like Amazon?

Finally, workers have had to fend off Amazon’s attacks. Generally, the company’s strategy toward unions has oscillated between largely ignoring workers’ efforts and actively bashing unions. While IP’s organizing did not disrupt the labor process at Amazon’s Polish warehouses, the slowdown in June 2015 had a marked impact, and a range of other factors — including the contentious collective bargaining process, open conflicts at Amazon employee assemblies, and a range of media reports critical of the IP — have sparked increasing tensions between Amazon’s management and IP’s shop stewards.

Amazon’s actions indicate that tension likely won’t dissipate any time soon. For instance, Amazon has offered the union an office off-site rather than, as mandated by Polish labor law, one on the company premises. While union shop stewards were initially able to use their “union hours” to meet at the workplace, Amazon has now prohibited this practice. And in recent weeks, several IP shop stewards have been transferred or given harder work tasks.

If IP’s Amazon section continues its organizing efforts, such reprisals will likely get even worse.

##Spreading the Struggle

Amazon is in the midst of expanding its warehouse and logistics network further into Eastern Europe. The company opened a new warehouse in Dobrovíz near Prague in the fall of 2015, and recently announced plans to open another warehouse in Poland. As such, cross-border organizing and solidarity is more important than ever.

Unfortunately, some union leaders don’t agree. The Verdi leadership has frequently tried to undermine the collaboration between Amazon workers in Poznań and Germany, arguing that Verdi members should work with Solidarność rather than the IP because the latter isn’t a Uniglobal member.

Meanwhile, Solidarność sees the IP as competition and publicly attacks it for its “radical” politics. If workers in Amazon warehouses in Poland and Germany are going to organize together, they’ll have to clear the obstacles union leaders throw in their way.

To that end, Amazon workers from Poland and Germany converged in Berlin last month for their most recent cross-border meeting. And they’ve begun reaching out to Amazon warehouse workers in countries like France, Italy, the Czech Republic, and Spain.

Amazon is a formidable foe, so they’ll need all the help they can get. But over the past year, Poznań workers have shown how solidarity and organizing have the potential to bring even a multinational giant to heel.
