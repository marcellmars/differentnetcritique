<!--
.. title: Control and Freedom: Power and Paranoia in the Age of Fiber Optics
.. slug: control-and_freedom
.. date: 2017-06-21 19:02:38 UTC+02:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

[DOWNLOAD BOOK](https://library.memoryoftheworld.org/b/CAx_epbujTXf0HbOXa-nOXprCX8pcbhkBIbjg7nL7tFZ1eeO)

# Control and Freedom

## Gawkers

The user, popularly understood as couch-potato-turned-anonymous- superagent, requires much online and off-line intervention. Browsers deliberately conceal the constant exchange between so-called clients and hosts through spinning globes and lighthouses at which we gaze as time goes by. Recent critical theorizations of the user as a flâneur or an ex- plorer similarly sustain the fiction of users as spectators rather than spec- tacles, or at least involuntary producers of information. Manovich, for instance, argues that flâneurs and explorers are the two major phenotypes for the user. Game users, navigating virtually empty yet adventure-filled spaces, mimic explorers in James Fenimore Cooper’s and Mark Twain’s fictions. The Web surfer is Charles-Pierre Baudelaire’s flâneur: a perfect spectator, who feels at home only while moving among crowds. The rein- carnated flâneur/data dandy, notes Manovich, ‘‘finds peace in the knowl- edge that she can slide over endless fields of data locating any morsel of information with the click of a button, zooming through file systems and networks, comforted by data manipulation operations at her control.’’ [^48] Although this trajectory is important, it is also important (as Manovich himself contends) to explore its limitations, especially since flâneurie depended in large part on Cooper’s portrayal of the frontier and since a flâneur would never post. To be a perfect spectator, one must see, but not be seen, ‘‘read’’ others and uncover their traces, but leave none of one’s own. As Baudelaire argues, For the perfect flâneur, . . . it is an immense joy to set up house in the heart of the multitude, amid the ebb and flow. . . . To be away from home, yet to feel oneself everywhere at home; to see the world, to be at the center of the world, yet to remain hidden from the world—such are a few of the slightest pleasures of those independent, passionate, impartial [!] natures which the tongue can but clumsily define. The spectator is a prince who everywhere rejoices in his incognito. 



[^48]: Manovich, *Language*, 274.
